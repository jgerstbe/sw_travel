var cities = [{
    "name": "Berlin",
    "einwohner": "3520031",
    "bundesland": "Berlin",
    "rank": "1"
}, {
    "name": "Hamburg",
    "einwohner": "1787408",
    "bundesland": "Hamburg",
    "rank": "2"
}, {
    "name": "München",
    "einwohner": "1450381",
    "bundesland": "Bayern",
    "rank": "3"
}, {
    "name": "Köln",
    "einwohner": "1060582",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "4"
}, {
    "name": "Frankfurt am Main",
    "einwohner": "732688",
    "bundesland": "Hessen",
    "rank": "5"
}, {
    "name": "Stuttgart",
    "einwohner": "623738",
    "bundesland": "Baden-Württemberg",
    "rank": "6"
}, {
    "name": "Düsseldorf",
    "einwohner": "612178",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "7"
}, {
    "name": "Dortmund",
    "einwohner": "586181",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "8"
}, {
    "name": "Essen",
    "einwohner": "582624",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "9"
}, {
    "name": "Leipzig",
    "einwohner": "560472",
    "bundesland": "Sachsen",
    "rank": "10"
}, {
    "name": "Bremen",
    "einwohner": "557464",
    "bundesland": "Bremen",
    "rank": "11"
}, {
    "name": "Dresden",
    "einwohner": "543825",
    "bundesland": "Sachsen",
    "rank": "12"
}, {
    "name": "Hannover",
    "einwohner": "532163",
    "bundesland": "Niedersachsen",
    "rank": "13"
}, {
    "name": "Nürnberg",
    "einwohner": "509975",
    "bundesland": "Bayern",
    "rank": "14"
}, {
    "name": "Duisburg",
    "einwohner": "491231",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "15"
}, {
    "name": "Bochum",
    "einwohner": "364742",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "16"
}, {
    "name": "Wuppertal",
    "einwohner": "350046",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "17"
}, {
    "name": "Bielefeld",
    "einwohner": "333090",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "18"
}, {
    "name": "Bonn",
    "einwohner": "318809",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "19"
}, {
    "name": "Münster",
    "einwohner": "310039",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "20"
}, {
    "name": "Karlsruhe",
    "einwohner": "307755",
    "bundesland": "Baden-Württemberg",
    "rank": "21"
}, {
    "name": "Mannheim",
    "einwohner": "305780",
    "bundesland": "Baden-Württemberg",
    "rank": "22"
}, {
    "name": "Augsburg",
    "einwohner": "286374",
    "bundesland": "Bayern",
    "rank": "23"
}, {
    "name": "Wiesbaden",
    "einwohner": "276218",
    "bundesland": "Hessen",
    "rank": "24"
}, {
    "name": "Gelsenkirchen",
    "einwohner": "260368",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "25"
}, {
    "name": "Mönchengladbach",
    "einwohner": "259996",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "26"
}, {
    "name": "Braunschweig",
    "einwohner": "251364",
    "bundesland": "Niedersachsen",
    "rank": "27"
}, {
    "name": "Chemnitz",
    "einwohner": "248645",
    "bundesland": "Sachsen",
    "rank": "28"
}, {
    "name": "Kiel",
    "einwohner": "246306",
    "bundesland": "Schleswig-Holstein",
    "rank": "29"
}, {
    "name": "Aachen",
    "einwohner": "245885",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "30"
}, {
    "name": "Halle (Saale)",
    "einwohner": "236991",
    "bundesland": "Sachsen-Anhalt",
    "rank": "31"
}, {
    "name": "Magdeburg",
    "einwohner": "235723",
    "bundesland": "Sachsen-Anhalt",
    "rank": "32"
}, {
    "name": "Freiburg",
    "einwohner": "226393",
    "bundesland": "Baden-Württemberg",
    "rank": "33"
}, {
    "name": "Krefeld",
    "einwohner": "225144",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "34"
}, {
    "name": "Lübeck",
    "einwohner": "216253",
    "bundesland": "Schleswig-Holstein",
    "rank": "35"
}, {
    "name": "Oberhausen",
    "einwohner": "210934",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "36"
}, {
    "name": "Erfurt",
    "einwohner": "210118",
    "bundesland": "Thüringen",
    "rank": "37"
}, {
    "name": "Mainz",
    "einwohner": "209779",
    "bundesland": "Rheinland-Pfalz",
    "rank": "38"
}, {
    "name": "Rostock",
    "einwohner": "206011",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "39"
}, {
    "name": "Kassel",
    "einwohner": "197984",
    "bundesland": "Hessen",
    "rank": "40"
}, {
    "name": "Hagen",
    "einwohner": "189044",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "41"
}, {
    "name": "Hamm",
    "einwohner": "179397",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "42"
}, {
    "name": "Saarbrücken",
    "einwohner": "178151",
    "bundesland": "Saarland",
    "rank": "43"
}, {
    "name": "Mülheim an der Ruhr",
    "einwohner": "169278",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "44"
}, {
    "name": "Potsdam",
    "einwohner": "167745",
    "bundesland": "Brandenburg",
    "rank": "45"
}, {
    "name": "Ludwigshafen am Rhein",
    "einwohner": "164718",
    "bundesland": "Rheinland-Pfalz",
    "rank": "46"
}, {
    "name": "Oldenburg",
    "einwohner": "163830",
    "bundesland": "Niedersachsen",
    "rank": "47"
}, {
    "name": "Leverkusen",
    "einwohner": "163487",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "48"
}, {
    "name": "Osnabrück",
    "einwohner": "162403",
    "bundesland": "Niedersachsen",
    "rank": "49"
}, {
    "name": "Solingen",
    "einwohner": "158726",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "50"
}, {
    "name": "Heidelberg",
    "einwohner": "156267",
    "bundesland": "Baden-Württemberg",
    "rank": "51"
}, {
    "name": "Herne",
    "einwohner": "155851",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "52"
}, {
    "name": "Neuss",
    "einwohner": "155414",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "53"
}, {
    "name": "Darmstadt",
    "einwohner": "155353",
    "bundesland": "Hessen",
    "rank": "54"
}, {
    "name": "Paderborn",
    "einwohner": "148126",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "55"
}, {
    "name": "Regensburg",
    "einwohner": "145465",
    "bundesland": "Bayern",
    "rank": "56"
}, {
    "name": "Ingolstadt",
    "einwohner": "132438",
    "bundesland": "Bayern",
    "rank": "57"
}, {
    "name": "Würzburg",
    "einwohner": "124873",
    "bundesland": "Bayern",
    "rank": "58"
}, {
    "name": "Fürth",
    "einwohner": "124171",
    "bundesland": "Bayern",
    "rank": "59"
}, {
    "name": "Wolfsburg",
    "einwohner": "124045",
    "bundesland": "Niedersachsen",
    "rank": "60"
}, {
    "name": "Offenbach am Main",
    "einwohner": "123734",
    "bundesland": "Hessen",
    "rank": "61"
}, {
    "name": "Ulm",
    "einwohner": "122636",
    "bundesland": "Baden-Württemberg",
    "rank": "62"
}, {
    "name": "Heilbronn",
    "einwohner": "122567",
    "bundesland": "Baden-Württemberg",
    "rank": "63"
}, {
    "name": "Pforzheim",
    "einwohner": "122247",
    "bundesland": "Baden-Württemberg",
    "rank": "64"
}, {
    "name": "Göttingen",
    "einwohner": "118914",
    "bundesland": "Niedersachsen",
    "rank": "65"
}, {
    "name": "Bottrop",
    "einwohner": "117143",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "66"
}, {
    "name": "Trier",
    "einwohner": "114914",
    "bundesland": "Rheinland-Pfalz",
    "rank": "67"
}, {
    "name": "Recklinghausen",
    "einwohner": "114330",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "68"
}, {
    "name": "Reutlingen",
    "einwohner": "114310",
    "bundesland": "Baden-Württemberg",
    "rank": "69"
}, {
    "name": "Bremerhaven",
    "einwohner": "114025",
    "bundesland": "Bremen",
    "rank": "70"
}, {
    "name": "Koblenz",
    "einwohner": "112586",
    "bundesland": "Rheinland-Pfalz",
    "rank": "71"
}, {
    "name": "Bergisch Gladbach",
    "einwohner": "111366",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "72"
}, {
    "name": "Jena",
    "einwohner": "109527",
    "bundesland": "Thüringen",
    "rank": "73"
}, {
    "name": "Remscheid",
    "einwohner": "109499",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "74"
}, {
    "name": "Erlangen",
    "einwohner": "108336",
    "bundesland": "Bayern",
    "rank": "75"
}, {
    "name": "Moers",
    "einwohner": "104529",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "76"
}, {
    "name": "Siegen",
    "einwohner": "102355",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "77"
}, {
    "name": "Hildesheim",
    "einwohner": "101667",
    "bundesland": "Niedersachsen",
    "rank": "78"
}, {
    "name": "Salzgitter",
    "einwohner": "101079",
    "bundesland": "Niedersachsen",
    "rank": "79"
}, {
    "name": "Cottbus",
    "einwohner": "99687",
    "bundesland": "Brandenburg",
    "rank": "80"
}, {
    "name": "Kaiserslautern",
    "einwohner": "98520",
    "bundesland": "Rheinland-Pfalz",
    "rank": "81"
}, {
    "name": "Gütersloh",
    "einwohner": "97586",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "82"
}, {
    "name": "Schwerin",
    "einwohner": "96800",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "83"
}, {
    "name": "Witten",
    "einwohner": "96700",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "84"
}, {
    "name": "Gera",
    "einwohner": "96011",
    "bundesland": "Thüringen",
    "rank": "85"
}, {
    "name": "Iserlohn",
    "einwohner": "93537",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "86"
}, {
    "name": "Ludwigsburg",
    "einwohner": "92973",
    "bundesland": "Baden-Württemberg",
    "rank": "87"
}, {
    "name": "Hanau",
    "einwohner": "92643",
    "bundesland": "Hessen",
    "rank": "88"
}, {
    "name": "Esslingen am Neckar",
    "einwohner": "91271",
    "bundesland": "Baden-Württemberg",
    "rank": "89"
}, {
    "name": "Zwickau",
    "einwohner": "91123",
    "bundesland": "Sachsen",
    "rank": "90"
}, {
    "name": "Düren",
    "einwohner": "90244",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "91"
}, {
    "name": "Ratingen",
    "einwohner": "87943",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "92"
}, {
    "name": "Tübingen",
    "einwohner": "87464",
    "bundesland": "Baden-Württemberg",
    "rank": "93"
}, {
    "name": "Flensburg",
    "einwohner": "85942",
    "bundesland": "Schleswig-Holstein",
    "rank": "94"
}, {
    "name": "Lünen",
    "einwohner": "85867",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "95"
}, {
    "name": "Villingen-Schwenningen",
    "einwohner": "84674",
    "bundesland": "Baden-Württemberg",
    "rank": "96"
}, {
    "name": "Gießen",
    "einwohner": "84455",
    "bundesland": "Hessen",
    "rank": "97"
}, {
    "name": "Marl",
    "einwohner": "83926",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "98"
}, {
    "name": "Dessau-Roßlau2",
    "einwohner": "82919",
    "bundesland": "Sachsen-Anhalt",
    "rank": "99"
}, {
    "name": "Konstanz",
    "einwohner": "82859",
    "bundesland": "Baden-Württemberg",
    "rank": "100"
}, {
    "name": "Worms",
    "einwohner": "82102",
    "bundesland": "Rheinland-Pfalz",
    "rank": "101"
}, {
    "name": "Minden",
    "einwohner": "81598",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "102"
}, {
    "name": "Velbert",
    "einwohner": "81430",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "103"
}, {
    "name": "Neumünster",
    "einwohner": "79197",
    "bundesland": "Schleswig-Holstein",
    "rank": "104"
}, {
    "name": "Norderstedt",
    "einwohner": "76712",
    "bundesland": "Schleswig-Holstein",
    "rank": "105"
}, {
    "name": "Delmenhorst",
    "einwohner": "76323",
    "bundesland": "Niedersachsen",
    "rank": "106"
}, {
    "name": "Wilhelmshaven",
    "einwohner": "75995",
    "bundesland": "Niedersachsen",
    "rank": "107"
}, {
    "name": "Viersen",
    "einwohner": "75931",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "108"
}, {
    "name": "Gladbeck",
    "einwohner": "75455",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "109"
}, {
    "name": "Dorsten",
    "einwohner": "75431",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "110"
}, {
    "name": "Rheine",
    "einwohner": "74852",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "111"
}, {
    "name": "Detmold",
    "einwohner": "74817",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "112"
}, {
    "name": "Troisdorf",
    "einwohner": "74400",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "113"
}, {
    "name": "Castrop-Rauxel",
    "einwohner": "74220",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "114"
}, {
    "name": "Lüneburg",
    "einwohner": "74072",
    "bundesland": "Niedersachsen",
    "rank": "115"
}, {
    "name": "Marburg",
    "einwohner": "73836",
    "bundesland": "Hessen",
    "rank": "116"
}, {
    "name": "Arnsberg",
    "einwohner": "73784",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "117"
}, {
    "name": "Lüdenscheid",
    "einwohner": "73354",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "118"
}, {
    "name": "Bamberg",
    "einwohner": "73331",
    "bundesland": "Bayern",
    "rank": "119"
}, {
    "name": "Bayreuth",
    "einwohner": "72148",
    "bundesland": "Bayern",
    "rank": "120"
}, {
    "name": "Brandenburg an der Havel",
    "einwohner": "71574",
    "bundesland": "Brandenburg",
    "rank": "121"
}, {
    "name": "Bocholt",
    "einwohner": "71443",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "122"
}, {
    "name": "Celle",
    "einwohner": "69748",
    "bundesland": "Niedersachsen",
    "rank": "123"
}, {
    "name": "Landshut",
    "einwohner": "69211",
    "bundesland": "Bayern",
    "rank": "124"
}, {
    "name": "Aschaffenburg",
    "einwohner": "68986",
    "bundesland": "Bayern",
    "rank": "125"
}, {
    "name": "Dinslaken",
    "einwohner": "67452",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "126"
}, {
    "name": "Aalen",
    "einwohner": "67344",
    "bundesland": "Baden-Württemberg",
    "rank": "127"
}, {
    "name": "Fulda",
    "einwohner": "67253",
    "bundesland": "Hessen",
    "rank": "128"
}, {
    "name": "Lippstadt",
    "einwohner": "67233",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "129"
}, {
    "name": "Kempten (Allgäu)",
    "einwohner": "66947",
    "bundesland": "Bayern",
    "rank": "130"
}, {
    "name": "Herford",
    "einwohner": "66521",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "131"
}, {
    "name": "Kerpen",
    "einwohner": "65477",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "132"
}, {
    "name": "Plauen",
    "einwohner": "65201",
    "bundesland": "Sachsen",
    "rank": "133"
}, {
    "name": "Neuwied",
    "einwohner": "64340",
    "bundesland": "Rheinland-Pfalz",
    "rank": "134"
}, {
    "name": "Weimar",
    "einwohner": "64131",
    "bundesland": "Thüringen",
    "rank": "135"
}, {
    "name": "Dormagen",
    "einwohner": "64064",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "136"
}, {
    "name": "Sindelfingen",
    "einwohner": "63971",
    "bundesland": "Baden-Württemberg",
    "rank": "137"
}, {
    "name": "Neubrandenburg",
    "einwohner": "63602",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "138"
}, {
    "name": "Grevenbroich",
    "einwohner": "63051",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "139"
}, {
    "name": "Rüsselsheim am Main",
    "einwohner": "63030",
    "bundesland": "Hessen",
    "rank": "140"
}, {
    "name": "Rosenheim",
    "einwohner": "61844",
    "bundesland": "Bayern",
    "rank": "141"
}, {
    "name": "Herten",
    "einwohner": "61163",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "142"
}, {
    "name": "Wesel",
    "einwohner": "60595",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "143"
}, {
    "name": "Garbsen",
    "einwohner": "60590",
    "bundesland": "Niedersachsen",
    "rank": "144"
}, {
    "name": "Bergheim",
    "einwohner": "60390",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "145"
}, {
    "name": "Schwäbisch Gmünd",
    "einwohner": "59840",
    "bundesland": "Baden-Württemberg",
    "rank": "146"
}, {
    "name": "Hürth",
    "einwohner": "59496",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "147"
}, {
    "name": "Unna",
    "einwohner": "59111",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "148"
}, {
    "name": "Friedrichshafen",
    "einwohner": "59108",
    "bundesland": "Baden-Württemberg",
    "rank": "149"
}, {
    "name": "Offenburg",
    "einwohner": "58465",
    "bundesland": "Baden-Württemberg",
    "rank": "150"
}, {
    "name": "Frankfurt (Oder)",
    "einwohner": "58092",
    "bundesland": "Brandenburg",
    "rank": "151"
}, {
    "name": "Stralsund",
    "einwohner": "58041",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "152"
}, {
    "name": "Langenfeld (Rheinland)",
    "einwohner": "58033",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "153"
}, {
    "name": "Greifswald",
    "einwohner": "57286",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "154"
}, {
    "name": "Neu-Ulm",
    "einwohner": "57237",
    "bundesland": "Bayern",
    "rank": "155"
}, {
    "name": "Göppingen",
    "einwohner": "56781",
    "bundesland": "Baden-Württemberg",
    "rank": "156"
}, {
    "name": "Euskirchen",
    "einwohner": "56769",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "157"
}, {
    "name": "Stolberg (Rheinland)",
    "einwohner": "56739",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "158"
}, {
    "name": "Hameln",
    "einwohner": "56529",
    "bundesland": "Niedersachsen",
    "rank": "159"
}, {
    "name": "Eschweiler",
    "einwohner": "55909",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "160"
}, {
    "name": "Sankt Augustin",
    "einwohner": "55709",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "161"
}, {
    "name": "Görlitz",
    "einwohner": "55255",
    "bundesland": "Sachsen",
    "rank": "162"
}, {
    "name": "Hilden",
    "einwohner": "55185",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "163"
}, {
    "name": "Meerbusch",
    "einwohner": "54892",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "164"
}, {
    "name": "Hattingen",
    "einwohner": "54834",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "165"
}, {
    "name": "Waiblingen",
    "einwohner": "54263",
    "bundesland": "Baden-Württemberg",
    "rank": "166"
}, {
    "name": "Pulheim",
    "einwohner": "54200",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "167"
}, {
    "name": "Baden-Baden",
    "einwohner": "54160",
    "bundesland": "Baden-Württemberg",
    "rank": "168"
}, {
    "name": "Menden (Sauerland)",
    "einwohner": "53485",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "169"
}, {
    "name": "Bad Salzuflen",
    "einwohner": "53341",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "170"
}, {
    "name": "Langenhagen",
    "einwohner": "53323",
    "bundesland": "Niedersachsen",
    "rank": "171"
}, {
    "name": "Nordhorn",
    "einwohner": "53285",
    "bundesland": "Niedersachsen",
    "rank": "172"
}, {
    "name": "Lingen (Ems)",
    "einwohner": "53284",
    "bundesland": "Niedersachsen",
    "rank": "173"
}, {
    "name": "Bad Homburg vor der Höhe",
    "einwohner": "53244",
    "bundesland": "Hessen",
    "rank": "174"
}, {
    "name": "Neustadt an der Weinstraße",
    "einwohner": "52999",
    "bundesland": "Rheinland-Pfalz",
    "rank": "175"
}, {
    "name": "Ahlen",
    "einwohner": "52287",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "176"
}, {
    "name": "Wolfenbüttel",
    "einwohner": "52269",
    "bundesland": "Niedersachsen",
    "rank": "177"
}, {
    "name": "Frechen",
    "einwohner": "51999",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "178"
}, {
    "name": "Schweinfurt",
    "einwohner": "51969",
    "bundesland": "Bayern",
    "rank": "179"
}, {
    "name": "Wetzlar",
    "einwohner": "51649",
    "bundesland": "Hessen",
    "rank": "180"
}, {
    "name": "Ibbenbüren",
    "einwohner": "50935",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "181"
}, {
    "name": "Goslar",
    "einwohner": "50782",
    "bundesland": "Niedersachsen",
    "rank": "182"
}, {
    "name": "Willich",
    "einwohner": "50748",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "183"
}, {
    "name": "Emden",
    "einwohner": "50694",
    "bundesland": "Niedersachsen",
    "rank": "184"
}, {
    "name": "Passau",
    "einwohner": "50566",
    "bundesland": "Bayern",
    "rank": "185"
}, {
    "name": "Gummersbach",
    "einwohner": "50412",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "186"
}, {
    "name": "Speyer",
    "einwohner": "50284",
    "bundesland": "Rheinland-Pfalz",
    "rank": "187"
}, {
    "name": "Ravensburg",
    "einwohner": "49830",
    "bundesland": "Baden-Württemberg",
    "rank": "188"
}, {
    "name": "Erftstadt",
    "einwohner": "49786",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "189"
}, {
    "name": "Kleve",
    "einwohner": "49729",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "190"
}, {
    "name": "Bad Kreuznach",
    "einwohner": "49406",
    "bundesland": "Rheinland-Pfalz",
    "rank": "191"
}, {
    "name": "Peine",
    "einwohner": "49366",
    "bundesland": "Niedersachsen",
    "rank": "192"
}, {
    "name": "Lörrach",
    "einwohner": "49303",
    "bundesland": "Baden-Württemberg",
    "rank": "193"
}, {
    "name": "Bad Oeynhausen",
    "einwohner": "48990",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "194"
}, {
    "name": "Böblingen",
    "einwohner": "48696",
    "bundesland": "Baden-Württemberg",
    "rank": "195"
}, {
    "name": "Elmshorn",
    "einwohner": "48684",
    "bundesland": "Schleswig-Holstein",
    "rank": "196"
}, {
    "name": "Frankenthal (Pfalz)",
    "einwohner": "48363",
    "bundesland": "Rheinland-Pfalz",
    "rank": "197"
}, {
    "name": "Cuxhaven",
    "einwohner": "48264",
    "bundesland": "Niedersachsen",
    "rank": "198"
}, {
    "name": "Rastatt",
    "einwohner": "48051",
    "bundesland": "Baden-Württemberg",
    "rank": "199"
}, {
    "name": "Heidenheim an der Brenz",
    "einwohner": "48048",
    "bundesland": "Baden-Württemberg",
    "rank": "200"
}, {
    "name": "Rheda-Wiedenbrück",
    "einwohner": "48000",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "201"
}, {
    "name": "Soest",
    "einwohner": "47974",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "202"
}, {
    "name": "Bergkamen",
    "einwohner": "47803",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "203"
}, {
    "name": "Bornheim",
    "einwohner": "47636",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "204"
}, {
    "name": "Singen (Hohentwiel)",
    "einwohner": "47287",
    "bundesland": "Baden-Württemberg",
    "rank": "205"
}, {
    "name": "Leonberg",
    "einwohner": "47219",
    "bundesland": "Baden-Württemberg",
    "rank": "206"
}, {
    "name": "Gronau (Westf)",
    "einwohner": "47010",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "207"
}, {
    "name": "Freising",
    "einwohner": "46963",
    "bundesland": "Bayern",
    "rank": "208"
}, {
    "name": "Hennef (Sieg)",
    "einwohner": "46902",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "209"
}, {
    "name": "Alsdorf",
    "einwohner": "46880",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "210"
}, {
    "name": "Straubing",
    "einwohner": "46806",
    "bundesland": "Bayern",
    "rank": "211"
}, {
    "name": "Schwerte",
    "einwohner": "46723",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "212"
}, {
    "name": "Dachau",
    "einwohner": "46705",
    "bundesland": "Bayern",
    "rank": "213"
}, {
    "name": "Dülmen",
    "einwohner": "46613",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "214"
}, {
    "name": "Herzogenrath",
    "einwohner": "46583",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "215"
}, {
    "name": "Wittenberg, Lutherstadt",
    "einwohner": "46475",
    "bundesland": "Sachsen-Anhalt",
    "rank": "216"
}, {
    "name": "Stade",
    "einwohner": "46378",
    "bundesland": "Niedersachsen",
    "rank": "217"
}, {
    "name": "Neunkirchen",
    "einwohner": "46369",
    "bundesland": "Saarland",
    "rank": "218"
}, {
    "name": "Melle",
    "einwohner": "46039",
    "bundesland": "Niedersachsen",
    "rank": "219"
}, {
    "name": "Oberursel (Taunus)",
    "einwohner": "45723",
    "bundesland": "Hessen",
    "rank": "220"
}, {
    "name": "Bünde",
    "einwohner": "45615",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "221"
}, {
    "name": "Gotha",
    "einwohner": "45410",
    "bundesland": "Thüringen",
    "rank": "222"
}, {
    "name": "Landau in der Pfalz",
    "einwohner": "45362",
    "bundesland": "Rheinland-Pfalz",
    "rank": "223"
}, {
    "name": "Fellbach",
    "einwohner": "45147",
    "bundesland": "Baden-Württemberg",
    "rank": "224"
}, {
    "name": "Filderstadt4",
    "einwohner": "44977",
    "bundesland": "Baden-Württemberg",
    "rank": "225"
}, {
    "name": "Lahr/Schwarzwald",
    "einwohner": "44884",
    "bundesland": "Baden-Württemberg",
    "rank": "226"
}, {
    "name": "Weinheim",
    "einwohner": "44797",
    "bundesland": "Baden-Württemberg",
    "rank": "227"
}, {
    "name": "Brühl",
    "einwohner": "44768",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "228"
}, {
    "name": "Hof",
    "einwohner": "44660",
    "bundesland": "Bayern",
    "rank": "229"
}, {
    "name": "Albstadt5",
    "einwohner": "44431",
    "bundesland": "Baden-Württemberg",
    "rank": "230"
}, {
    "name": "Rodgau6",
    "einwohner": "44222",
    "bundesland": "Hessen",
    "rank": "231"
}, {
    "name": "Bruchsal",
    "einwohner": "44104",
    "bundesland": "Baden-Württemberg",
    "rank": "232"
}, {
    "name": "Erkrath",
    "einwohner": "44086",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "233"
}, {
    "name": "Neustadt am Rübenberge",
    "einwohner": "43931",
    "bundesland": "Niedersachsen",
    "rank": "234"
}, {
    "name": "Kamen",
    "einwohner": "43868",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "235"
}, {
    "name": "Halberstadt",
    "einwohner": "43768",
    "bundesland": "Sachsen-Anhalt",
    "rank": "236"
}, {
    "name": "Lehrte",
    "einwohner": "43639",
    "bundesland": "Niedersachsen",
    "rank": "237"
}, {
    "name": "Oranienburg",
    "einwohner": "43526",
    "bundesland": "Brandenburg",
    "rank": "238"
}, {
    "name": "Erkelenz",
    "einwohner": "43350",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "239"
}, {
    "name": "Kaarst",
    "einwohner": "43286",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "240"
}, {
    "name": "Rottenburg am Neckar",
    "einwohner": "43278",
    "bundesland": "Baden-Württemberg",
    "rank": "241"
}, {
    "name": "Bietigheim-Bissingen7",
    "einwohner": "42968",
    "bundesland": "Baden-Württemberg",
    "rank": "242"
}, {
    "name": "Memmingen",
    "einwohner": "42841",
    "bundesland": "Bayern",
    "rank": "243"
}, {
    "name": "Kaufbeuren",
    "einwohner": "42731",
    "bundesland": "Bayern",
    "rank": "244"
}, {
    "name": "Falkensee",
    "einwohner": "42634",
    "bundesland": "Brandenburg",
    "rank": "245"
}, {
    "name": "Wismar",
    "einwohner": "42557",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "246"
}, {
    "name": "Eisenach",
    "einwohner": "42417",
    "bundesland": "Thüringen",
    "rank": "247"
}, {
    "name": "Borken",
    "einwohner": "42272",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "248"
}, {
    "name": "Pinneberg",
    "einwohner": "42266",
    "bundesland": "Schleswig-Holstein",
    "rank": "249"
}, {
    "name": "Nordhausen",
    "einwohner": "42217",
    "bundesland": "Thüringen",
    "rank": "250"
}, {
    "name": "Weiden in der Oberpfalz",
    "einwohner": "42055",
    "bundesland": "Bayern",
    "rank": "251"
}, {
    "name": "Homburg",
    "einwohner": "41974",
    "bundesland": "Saarland",
    "rank": "252"
}, {
    "name": "Nettetal",
    "einwohner": "41964",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "253"
}, {
    "name": "Gifhorn",
    "einwohner": "41905",
    "bundesland": "Niedersachsen",
    "rank": "254"
}, {
    "name": "Amberg",
    "einwohner": "41861",
    "bundesland": "Bayern",
    "rank": "255"
}, {
    "name": "Freiberg",
    "einwohner": "41641",
    "bundesland": "Sachsen",
    "rank": "256"
}, {
    "name": "Heinsberg",
    "einwohner": "41538",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "257"
}, {
    "name": "Aurich",
    "einwohner": "41489",
    "bundesland": "Niedersachsen",
    "rank": "258"
}, {
    "name": "Lemgo",
    "einwohner": "41276",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "259"
}, {
    "name": "Coburg",
    "einwohner": "41257",
    "bundesland": "Bayern",
    "rank": "260"
}, {
    "name": "Wunstorf",
    "einwohner": "41251",
    "bundesland": "Niedersachsen",
    "rank": "261"
}, {
    "name": "Ansbach",
    "einwohner": "41159",
    "bundesland": "Bayern",
    "rank": "262"
}, {
    "name": "Siegburg",
    "einwohner": "41016",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "263"
}, {
    "name": "Seevetal(*) 9",
    "einwohner": "40949",
    "bundesland": "Niedersachsen",
    "rank": "264"
}, {
    "name": "Laatzen",
    "einwohner": "40939",
    "bundesland": "Niedersachsen",
    "rank": "265"
}, {
    "name": "Monheim am Rhein",
    "einwohner": "40885",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "266"
}, {
    "name": "Königswinter",
    "einwohner": "40702",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "267"
}, {
    "name": "Dreieich10",
    "einwohner": "40601",
    "bundesland": "Hessen",
    "rank": "268"
}, {
    "name": "Nürtingen",
    "einwohner": "40535",
    "bundesland": "Baden-Württemberg",
    "rank": "269"
}, {
    "name": "Bitterfeld-Wolfen8",
    "einwohner": "40480",
    "bundesland": "Sachsen-Anhalt",
    "rank": "270"
}, {
    "name": "Schwabach",
    "einwohner": "40428",
    "bundesland": "Bayern",
    "rank": "271"
}, {
    "name": "Weißenfels",
    "einwohner": "40398",
    "bundesland": "Sachsen-Anhalt",
    "rank": "272"
}, {
    "name": "Stendal",
    "einwohner": "40269",
    "bundesland": "Sachsen-Anhalt",
    "rank": "273"
}, {
    "name": "Buxtehude",
    "einwohner": "40173",
    "bundesland": "Niedersachsen",
    "rank": "274"
}, {
    "name": "Pirmasens",
    "einwohner": "40125",
    "bundesland": "Rheinland-Pfalz",
    "rank": "275"
}, {
    "name": "Kirchheim unter Teck",
    "einwohner": "40094",
    "bundesland": "Baden-Württemberg",
    "rank": "276"
}, {
    "name": "Löhne",
    "einwohner": "40086",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "277"
}, {
    "name": "Bensheim",
    "einwohner": "40051",
    "bundesland": "Hessen",
    "rank": "278"
}, {
    "name": "Bautzen",
    "einwohner": "39845",
    "bundesland": "Sachsen",
    "rank": "279"
}, {
    "name": "Freital",
    "einwohner": "39734",
    "bundesland": "Sachsen",
    "rank": "280"
}, {
    "name": "Hückelhoven11",
    "einwohner": "39531",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "281"
}, {
    "name": "Hofheim am Taunus",
    "einwohner": "39476",
    "bundesland": "Hessen",
    "rank": "282"
}, {
    "name": "Germering",
    "einwohner": "39387",
    "bundesland": "Bayern",
    "rank": "283"
}, {
    "name": "Neumarkt in der Oberpfalz",
    "einwohner": "39333",
    "bundesland": "Bayern",
    "rank": "284"
}, {
    "name": "Eberswalde12",
    "einwohner": "39303",
    "bundesland": "Brandenburg",
    "rank": "285"
}, {
    "name": "Ahaus",
    "einwohner": "39277",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "286"
}, {
    "name": "Schorndorf",
    "einwohner": "39172",
    "bundesland": "Baden-Württemberg",
    "rank": "287"
}, {
    "name": "Völklingen",
    "einwohner": "39129",
    "bundesland": "Saarland",
    "rank": "288"
}, {
    "name": "Leinfelden-Echterdingen13",
    "einwohner": "39071",
    "bundesland": "Baden-Württemberg",
    "rank": "289"
}, {
    "name": "Ettlingen",
    "einwohner": "38982",
    "bundesland": "Baden-Württemberg",
    "rank": "290"
}, {
    "name": "Würselen",
    "einwohner": "38962",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "291"
}, {
    "name": "Schwäbisch Hall",
    "einwohner": "38827",
    "bundesland": "Baden-Württemberg",
    "rank": "292"
}, {
    "name": "Ostfildern14",
    "einwohner": "38519",
    "bundesland": "Baden-Württemberg",
    "rank": "293"
}, {
    "name": "Buchholz in der Nordheide",
    "einwohner": "38487",
    "bundesland": "Niedersachsen",
    "rank": "294"
}, {
    "name": "Mettmann",
    "einwohner": "38291",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "295"
}, {
    "name": "Maintal15",
    "einwohner": "38208",
    "bundesland": "Hessen",
    "rank": "296"
}, {
    "name": "Haltern am See",
    "einwohner": "38020",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "297"
}, {
    "name": "Pirna",
    "einwohner": "38010",
    "bundesland": "Sachsen",
    "rank": "298"
}, {
    "name": "Kamp-Lintfort",
    "einwohner": "37683",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "299"
}, {
    "name": "Niederkassel",
    "einwohner": "37583",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "300"
}, {
    "name": "Neu-Isenburg",
    "einwohner": "37565",
    "bundesland": "Hessen",
    "rank": "301"
}, {
    "name": "Warendorf",
    "einwohner": "37249",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "302"
}, {
    "name": "Bernau bei Berlin",
    "einwohner": "37169",
    "bundesland": "Brandenburg",
    "rank": "303"
}, {
    "name": "Langen",
    "einwohner": "37026",
    "bundesland": "Hessen",
    "rank": "304"
}, {
    "name": "Suhl",
    "einwohner": "36778",
    "bundesland": "Thüringen",
    "rank": "305"
}, {
    "name": "Papenburg",
    "einwohner": "36690",
    "bundesland": "Niedersachsen",
    "rank": "306"
}, {
    "name": "Voerde (Niederrhein)",
    "einwohner": "36675",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "307"
}, {
    "name": "Greven",
    "einwohner": "36674",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "308"
}, {
    "name": "Beckum",
    "einwohner": "36560",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "309"
}, {
    "name": "Emsdetten",
    "einwohner": "36320",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "310"
}, {
    "name": "Sankt Ingbert",
    "einwohner": "36292",
    "bundesland": "Saarland",
    "rank": "311"
}, {
    "name": "Backnang",
    "einwohner": "36266",
    "bundesland": "Baden-Württemberg",
    "rank": "312"
}, {
    "name": "Erding",
    "einwohner": "36159",
    "bundesland": "Bayern",
    "rank": "313"
}, {
    "name": "Coesfeld",
    "einwohner": "36116",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "314"
}, {
    "name": "Bramsche",
    "einwohner": "36013",
    "bundesland": "Niedersachsen",
    "rank": "315"
}, {
    "name": "Wesseling",
    "einwohner": "35975",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "316"
}, {
    "name": "Königs Wusterhausen",
    "einwohner": "35765",
    "bundesland": "Brandenburg",
    "rank": "317"
}, {
    "name": "Fürstenfeldbruck",
    "einwohner": "35708",
    "bundesland": "Bayern",
    "rank": "318"
}, {
    "name": "Porta Westfalica16",
    "einwohner": "35430",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "319"
}, {
    "name": "Sinsheim",
    "einwohner": "35175",
    "bundesland": "Baden-Württemberg",
    "rank": "320"
}, {
    "name": "Lage",
    "einwohner": "35120",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "321"
}, {
    "name": "Kehl",
    "einwohner": "35032",
    "bundesland": "Baden-Württemberg",
    "rank": "322"
}, {
    "name": "Meppen",
    "einwohner": "34918",
    "bundesland": "Niedersachsen",
    "rank": "323"
}, {
    "name": "Kempen",
    "einwohner": "34837",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "324"
}, {
    "name": "Saarlouis",
    "einwohner": "34768",
    "bundesland": "Saarland",
    "rank": "325"
}, {
    "name": "Tuttlingen",
    "einwohner": "34586",
    "bundesland": "Baden-Württemberg",
    "rank": "326"
}, {
    "name": "Mühlhausen/Thüringen",
    "einwohner": "34552",
    "bundesland": "Thüringen",
    "rank": "327"
}, {
    "name": "Datteln",
    "einwohner": "34521",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "328"
}, {
    "name": "Wermelskirchen",
    "einwohner": "34504",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "329"
}, {
    "name": "Zweibrücken",
    "einwohner": "34260",
    "bundesland": "Rheinland-Pfalz",
    "rank": "330"
}, {
    "name": "Limburg an der Lahn",
    "einwohner": "34255",
    "bundesland": "Hessen",
    "rank": "331"
}, {
    "name": "Viernheim",
    "einwohner": "34146",
    "bundesland": "Hessen",
    "rank": "332"
}, {
    "name": "Radebeul",
    "einwohner": "34055",
    "bundesland": "Sachsen",
    "rank": "333"
}, {
    "name": "Merseburg",
    "einwohner": "34052",
    "bundesland": "Sachsen-Anhalt",
    "rank": "334"
}, {
    "name": "Leer",
    "einwohner": "34042",
    "bundesland": "Niedersachsen",
    "rank": "335"
}, {
    "name": "Bernburg (Saale)",
    "einwohner": "33920",
    "bundesland": "Sachsen-Anhalt",
    "rank": "336"
}, {
    "name": "Winsen (Luhe)",
    "einwohner": "33896",
    "bundesland": "Niedersachsen",
    "rank": "337"
}, {
    "name": "Goch",
    "einwohner": "33889",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "338"
}, {
    "name": "Hoyerswerda",
    "einwohner": "33843",
    "bundesland": "Sachsen",
    "rank": "339"
}, {
    "name": "Geldern",
    "einwohner": "33841",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "340"
}, {
    "name": "Cloppenburg",
    "einwohner": "33798",
    "bundesland": "Niedersachsen",
    "rank": "341"
}, {
    "name": "Uelzen",
    "einwohner": "33782",
    "bundesland": "Niedersachsen",
    "rank": "342"
}, {
    "name": "Crailsheim",
    "einwohner": "33768",
    "bundesland": "Baden-Württemberg",
    "rank": "343"
}, {
    "name": "Steinfurt17",
    "einwohner": "33682",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "344"
}, {
    "name": "Balingen",
    "einwohner": "33640",
    "bundesland": "Baden-Württemberg",
    "rank": "345"
}, {
    "name": "Mörfelden-Walldorf18",
    "einwohner": "33623",
    "bundesland": "Hessen",
    "rank": "346"
}, {
    "name": "Barsinghausen",
    "einwohner": "33608",
    "bundesland": "Niedersachsen",
    "rank": "347"
}, {
    "name": "Hemer",
    "einwohner": "33535",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "348"
}, {
    "name": "Dietzenbach",
    "einwohner": "33397",
    "bundesland": "Hessen",
    "rank": "349"
}, {
    "name": "Seelze",
    "einwohner": "33283",
    "bundesland": "Niedersachsen",
    "rank": "350"
}, {
    "name": "Kornwestheim",
    "einwohner": "33153",
    "bundesland": "Baden-Württemberg",
    "rank": "351"
}, {
    "name": "Wernigerode",
    "einwohner": "33108",
    "bundesland": "Sachsen-Anhalt",
    "rank": "352"
}, {
    "name": "Stuhr(*)",
    "einwohner": "33083",
    "bundesland": "Niedersachsen",
    "rank": "353"
}, {
    "name": "Bad Vilbel",
    "einwohner": "33020",
    "bundesland": "Hessen",
    "rank": "354"
}, {
    "name": "Naumburg (Saale)",
    "einwohner": "33012",
    "bundesland": "Sachsen-Anhalt",
    "rank": "355"
}, {
    "name": "Korschenbroich",
    "einwohner": "32922",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "356"
}, {
    "name": "Altenburg",
    "einwohner": "32910",
    "bundesland": "Thüringen",
    "rank": "357"
}, {
    "name": "Wedel",
    "einwohner": "32890",
    "bundesland": "Schleswig-Holstein",
    "rank": "358"
}, {
    "name": "Rheinfelden (Baden)",
    "einwohner": "32756",
    "bundesland": "Baden-Württemberg",
    "rank": "359"
}, {
    "name": "Ahrensburg",
    "einwohner": "32606",
    "bundesland": "Schleswig-Holstein",
    "rank": "360"
}, {
    "name": "Jülich",
    "einwohner": "32601",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "361"
}, {
    "name": "Lampertheim",
    "einwohner": "32303",
    "bundesland": "Hessen",
    "rank": "362"
}, {
    "name": "Biberach an der Riß",
    "einwohner": "32233",
    "bundesland": "Baden-Württemberg",
    "rank": "363"
}, {
    "name": "Deggendorf",
    "einwohner": "32189",
    "bundesland": "Bayern",
    "rank": "364"
}, {
    "name": "Delbrück",
    "einwohner": "31964",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "365"
}, {
    "name": "Itzehoe",
    "einwohner": "31771",
    "bundesland": "Schleswig-Holstein",
    "rank": "366"
}, {
    "name": "Fürstenwalde/Spree",
    "einwohner": "31741",
    "bundesland": "Brandenburg",
    "rank": "367"
}, {
    "name": "Forchheim",
    "einwohner": "31651",
    "bundesland": "Bayern",
    "rank": "368"
}, {
    "name": "Bad Nauheim",
    "einwohner": "31630",
    "bundesland": "Hessen",
    "rank": "369"
}, {
    "name": "Vechta",
    "einwohner": "31558",
    "bundesland": "Niedersachsen",
    "rank": "370"
}, {
    "name": "Georgsmarienhütte",
    "einwohner": "31520",
    "bundesland": "Niedersachsen",
    "rank": "371"
}, {
    "name": "Kreuztal",
    "einwohner": "31500",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "372"
}, {
    "name": "Oer-Erkenschwick",
    "einwohner": "31387",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "373"
}, {
    "name": "Schönebeck",
    "einwohner": "31366",
    "bundesland": "Sachsen-Anhalt",
    "rank": "374"
}, {
    "name": "Einbeck",
    "einwohner": "31338",
    "bundesland": "Niedersachsen",
    "rank": "375"
}, {
    "name": "Gevelsberg",
    "einwohner": "31315",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "376"
}, {
    "name": "Nienburg/Weser",
    "einwohner": "31193",
    "bundesland": "Niedersachsen",
    "rank": "377"
}, {
    "name": "Achim",
    "einwohner": "31156",
    "bundesland": "Niedersachsen",
    "rank": "378"
}, {
    "name": "Rheinberg",
    "einwohner": "31023",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "379"
}, {
    "name": "Herrenberg",
    "einwohner": "31003",
    "bundesland": "Baden-Württemberg",
    "rank": "380"
}, {
    "name": "Emmerich am Rhein",
    "einwohner": "30968",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "381"
}, {
    "name": "Ganderkesee(*)",
    "einwohner": "30943",
    "bundesland": "Niedersachsen",
    "rank": "382"
}, {
    "name": "Radolfzell am Bodensee",
    "einwohner": "30943",
    "bundesland": "Baden-Württemberg",
    "rank": "383"
}, {
    "name": "Geestland19",
    "einwohner": "30936",
    "bundesland": "Niedersachsen",
    "rank": "384"
}, {
    "name": "Riesa",
    "einwohner": "30885",
    "bundesland": "Sachsen",
    "rank": "385"
}, {
    "name": "Neuruppin",
    "einwohner": "30715",
    "bundesland": "Brandenburg",
    "rank": "386"
}, {
    "name": "Werl",
    "einwohner": "30638",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "387"
}, {
    "name": "Eisenhüttenstadt",
    "einwohner": "30416",
    "bundesland": "Brandenburg",
    "rank": "388"
}, {
    "name": "Haan",
    "einwohner": "30410",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "389"
}, {
    "name": "Weyhe(*) 20",
    "einwohner": "30388",
    "bundesland": "Niedersachsen",
    "rank": "390"
}, {
    "name": "Lohmar",
    "einwohner": "30348",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "391"
}, {
    "name": "Osterholz-Scharmbeck",
    "einwohner": "30302",
    "bundesland": "Niedersachsen",
    "rank": "392"
}, {
    "name": "Schwedt/Oder",
    "einwohner": "30262",
    "bundesland": "Brandenburg",
    "rank": "393"
}, {
    "name": "Burgdorf",
    "einwohner": "30147",
    "bundesland": "Niedersachsen",
    "rank": "394"
}, {
    "name": "Meschede",
    "einwohner": "30119",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "395"
}, {
    "name": "Weil am Rhein",
    "einwohner": "30030",
    "bundesland": "Baden-Württemberg",
    "rank": "396"
}, {
    "name": "Geesthacht",
    "einwohner": "30030",
    "bundesland": "Schleswig-Holstein",
    "rank": "397"
}, {
    "name": "Werne",
    "einwohner": "29955",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "398"
}, {
    "name": "Merzig",
    "einwohner": "29937",
    "bundesland": "Saarland",
    "rank": "399"
}, {
    "name": "Ennepetal",
    "einwohner": "29926",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "400"
}, {
    "name": "Höxter",
    "einwohner": "29589",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "401"
}, {
    "name": "Andernach",
    "einwohner": "29441",
    "bundesland": "Rheinland-Pfalz",
    "rank": "402"
}, {
    "name": "Rietberg",
    "einwohner": "29436",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "403"
}, {
    "name": "Wedemark(*) 21",
    "einwohner": "29358",
    "bundesland": "Niedersachsen",
    "rank": "404"
}, {
    "name": "Waltrop",
    "einwohner": "29354",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "405"
}, {
    "name": "Friedberg",
    "einwohner": "29339",
    "bundesland": "Bayern",
    "rank": "406"
}, {
    "name": "Oelde",
    "einwohner": "29299",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "407"
}, {
    "name": "Tönisvorst",
    "einwohner": "29296",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "408"
}, {
    "name": "Neuburg an der Donau",
    "einwohner": "29182",
    "bundesland": "Bayern",
    "rank": "409"
}, {
    "name": "Bad Hersfeld",
    "einwohner": "29116",
    "bundesland": "Hessen",
    "rank": "410"
}, {
    "name": "Taunusstein",
    "einwohner": "29063",
    "bundesland": "Hessen",
    "rank": "411"
}, {
    "name": "Zeitz",
    "einwohner": "29052",
    "bundesland": "Sachsen-Anhalt",
    "rank": "412"
}, {
    "name": "Gaggenau",
    "einwohner": "29032",
    "bundesland": "Baden-Württemberg",
    "rank": "413"
}, {
    "name": "Northeim",
    "einwohner": "28920",
    "bundesland": "Niedersachsen",
    "rank": "414"
}, {
    "name": "Bühl",
    "einwohner": "28882",
    "bundesland": "Baden-Württemberg",
    "rank": "415"
}, {
    "name": "Güstrow",
    "einwohner": "28845",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "416"
}, {
    "name": "Bretten",
    "einwohner": "28826",
    "bundesland": "Baden-Württemberg",
    "rank": "417"
}, {
    "name": "Landsberg am Lech",
    "einwohner": "28708",
    "bundesland": "Bayern",
    "rank": "418"
}, {
    "name": "Vaihingen an der Enz",
    "einwohner": "28695",
    "bundesland": "Baden-Württemberg",
    "rank": "419"
}, {
    "name": "Springe",
    "einwohner": "28682",
    "bundesland": "Niedersachsen",
    "rank": "420"
}, {
    "name": "Schwandorf",
    "einwohner": "28481",
    "bundesland": "Bayern",
    "rank": "421"
}, {
    "name": "Grimma",
    "einwohner": "28480",
    "bundesland": "Sachsen",
    "rank": "422"
}, {
    "name": "Kelkheim (Taunus)",
    "einwohner": "28452",
    "bundesland": "Hessen",
    "rank": "423"
}, {
    "name": "Rösrath",
    "einwohner": "28386",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "424"
}, {
    "name": "Idar-Oberstein",
    "einwohner": "28350",
    "bundesland": "Rheinland-Pfalz",
    "rank": "425"
}, {
    "name": "Schwelm",
    "einwohner": "28330",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "426"
}, {
    "name": "Kevelaer",
    "einwohner": "28311",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "427"
}, {
    "name": "Bad Zwischenahn(*) 22",
    "einwohner": "28204",
    "bundesland": "Niedersachsen",
    "rank": "428"
}, {
    "name": "Mühlheim am Main",
    "einwohner": "28170",
    "bundesland": "Hessen",
    "rank": "429"
}, {
    "name": "Sundern (Sauerland)",
    "einwohner": "28166",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "430"
}, {
    "name": "Friedberg (Hessen)",
    "einwohner": "28156",
    "bundesland": "Hessen",
    "rank": "431"
}, {
    "name": "Unterschleißheim",
    "einwohner": "28051",
    "bundesland": "Bayern",
    "rank": "432"
}, {
    "name": "Leichlingen (Rheinland)",
    "einwohner": "27937",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "433"
}, {
    "name": "Meißen",
    "einwohner": "27936",
    "bundesland": "Sachsen",
    "rank": "434"
}, {
    "name": "Winnenden",
    "einwohner": "27932",
    "bundesland": "Baden-Württemberg",
    "rank": "435"
}, {
    "name": "Wegberg",
    "einwohner": "27827",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "436"
}, {
    "name": "Wetter (Ruhr)",
    "einwohner": "27822",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "437"
}, {
    "name": "Aschersleben",
    "einwohner": "27793",
    "bundesland": "Sachsen-Anhalt",
    "rank": "438"
}, {
    "name": "Königsbrunn",
    "einwohner": "27772",
    "bundesland": "Bayern",
    "rank": "439"
}, {
    "name": "Sangerhausen",
    "einwohner": "27752",
    "bundesland": "Sachsen-Anhalt",
    "rank": "440"
}, {
    "name": "Henstedt-Ulzburg(*)",
    "einwohner": "27705",
    "bundesland": "Schleswig-Holstein",
    "rank": "441"
}, {
    "name": "Baunatal",
    "einwohner": "27617",
    "bundesland": "Hessen",
    "rank": "442"
}, {
    "name": "Rendsburg",
    "einwohner": "27617",
    "bundesland": "Schleswig-Holstein",
    "rank": "443"
}, {
    "name": "Bad Neuenahr-Ahrweiler",
    "einwohner": "27468",
    "bundesland": "Rheinland-Pfalz",
    "rank": "444"
}, {
    "name": "Emmendingen",
    "einwohner": "27383",
    "bundesland": "Baden-Württemberg",
    "rank": "445"
}, {
    "name": "Overath",
    "einwohner": "27264",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "446"
}, {
    "name": "Rödermark23",
    "einwohner": "27242",
    "bundesland": "Hessen",
    "rank": "447"
}, {
    "name": "Rheinbach",
    "einwohner": "27224",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "448"
}, {
    "name": "Neukirchen-Vluyn",
    "einwohner": "27178",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "449"
}, {
    "name": "Mechernich",
    "einwohner": "27170",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "450"
}, {
    "name": "Geislingen an der Steige",
    "einwohner": "27168",
    "bundesland": "Baden-Württemberg",
    "rank": "451"
}, {
    "name": "Wangen im Allgäu",
    "einwohner": "27093",
    "bundesland": "Baden-Württemberg",
    "rank": "452"
}, {
    "name": "Schloß Holte-Stukenbrock",
    "einwohner": "27092",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "453"
}, {
    "name": "Reinbek",
    "einwohner": "27048",
    "bundesland": "Schleswig-Holstein",
    "rank": "454"
}, {
    "name": "Verden (Aller)",
    "einwohner": "26997",
    "bundesland": "Niedersachsen",
    "rank": "455"
}, {
    "name": "Hamminkeln",
    "einwohner": "26996",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "456"
}, {
    "name": "Geilenkirchen",
    "einwohner": "26963",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "457"
}, {
    "name": "Leimen",
    "einwohner": "26910",
    "bundesland": "Baden-Württemberg",
    "rank": "458"
}, {
    "name": "Hattersheim am Main",
    "einwohner": "26908",
    "bundesland": "Hessen",
    "rank": "459"
}, {
    "name": "Griesheim",
    "einwohner": "26907",
    "bundesland": "Hessen",
    "rank": "460"
}, {
    "name": "Olching",
    "einwohner": "26825",
    "bundesland": "Bayern",
    "rank": "461"
}, {
    "name": "Garmisch-Partenkirchen(*)",
    "einwohner": "26821",
    "bundesland": "Bayern",
    "rank": "462"
}, {
    "name": "Baesweiler",
    "einwohner": "26819",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "463"
}, {
    "name": "Weinstadt24",
    "einwohner": "26685",
    "bundesland": "Baden-Württemberg",
    "rank": "464"
}, {
    "name": "Staßfurt",
    "einwohner": "26634",
    "bundesland": "Sachsen-Anhalt",
    "rank": "465"
}, {
    "name": "Selm",
    "einwohner": "26603",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "466"
}, {
    "name": "Ellwangen (Jagst)",
    "einwohner": "26574",
    "bundesland": "Baden-Württemberg",
    "rank": "467"
}, {
    "name": "Ingelheim am Rhein",
    "einwohner": "26546",
    "bundesland": "Rheinland-Pfalz",
    "rank": "468"
}, {
    "name": "Köthen (Anhalt)",
    "einwohner": "26519",
    "bundesland": "Sachsen-Anhalt",
    "rank": "469"
}, {
    "name": "Wiesloch",
    "einwohner": "26426",
    "bundesland": "Baden-Württemberg",
    "rank": "470"
}, {
    "name": "Lauf an der Pegnitz",
    "einwohner": "26344",
    "bundesland": "Bayern",
    "rank": "471"
}, {
    "name": "Nordenham",
    "einwohner": "26325",
    "bundesland": "Niedersachsen",
    "rank": "472"
}, {
    "name": "Blankenfelde-Mahlow(*) 25",
    "einwohner": "26319",
    "bundesland": "Brandenburg",
    "rank": "473"
}, {
    "name": "Neckarsulm",
    "einwohner": "26304",
    "bundesland": "Baden-Württemberg",
    "rank": "474"
}, {
    "name": "Hennigsdorf",
    "einwohner": "26264",
    "bundesland": "Brandenburg",
    "rank": "475"
}, {
    "name": "Brilon",
    "einwohner": "26232",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "476"
}, {
    "name": "Strausberg",
    "einwohner": "26213",
    "bundesland": "Brandenburg",
    "rank": "477"
}, {
    "name": "Ilmenau",
    "einwohner": "26153",
    "bundesland": "Thüringen",
    "rank": "478"
}, {
    "name": "Lennestadt",
    "einwohner": "26073",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "479"
}, {
    "name": "St Wendel",
    "einwohner": "26066",
    "bundesland": "Saarland",
    "rank": "480"
}, {
    "name": "Lohne (Oldenburg)",
    "einwohner": "26060",
    "bundesland": "Niedersachsen",
    "rank": "481"
}, {
    "name": "Kulmbach",
    "einwohner": "25933",
    "bundesland": "Bayern",
    "rank": "482"
}, {
    "name": "Heiligenhaus",
    "einwohner": "25793",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "483"
}, {
    "name": "Plettenberg",
    "einwohner": "25781",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "484"
}, {
    "name": "Remseck am Neckar26",
    "einwohner": "25759",
    "bundesland": "Baden-Württemberg",
    "rank": "485"
}, {
    "name": "Zittau",
    "einwohner": "25712",
    "bundesland": "Sachsen",
    "rank": "486"
}, {
    "name": "Petershagen",
    "einwohner": "25663",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "487"
}, {
    "name": "Bad Honnef",
    "einwohner": "25654",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "488"
}, {
    "name": "Mühlacker",
    "einwohner": "25649",
    "bundesland": "Baden-Württemberg",
    "rank": "489"
}, {
    "name": "Butzbach",
    "einwohner": "25557",
    "bundesland": "Hessen",
    "rank": "490"
}, {
    "name": "Hohen Neuendorf",
    "einwohner": "25519",
    "bundesland": "Brandenburg",
    "rank": "491"
}, {
    "name": "Verl",
    "einwohner": "25512",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "492"
}, {
    "name": "Ehingen",
    "einwohner": "25503",
    "bundesland": "Baden-Württemberg",
    "rank": "493"
}, {
    "name": "Teltow",
    "einwohner": "25483",
    "bundesland": "Brandenburg",
    "rank": "494"
}, {
    "name": "Lübbecke",
    "einwohner": "25462",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "495"
}, {
    "name": "Weiterstadt",
    "einwohner": "25416",
    "bundesland": "Hessen",
    "rank": "496"
}, {
    "name": "Warstein",
    "einwohner": "25407",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "497"
}, {
    "name": "Heppenheim (Bergstraße)",
    "einwohner": "25284",
    "bundesland": "Hessen",
    "rank": "498"
}, {
    "name": "Wiehl",
    "einwohner": "25274",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "499"
}, {
    "name": "Schmallenberg",
    "einwohner": "25230",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "500"
}, {
    "name": "Pfaffenhofen an der Ilm",
    "einwohner": "25226",
    "bundesland": "Bayern",
    "rank": "501"
}, {
    "name": "Sprockhövel",
    "einwohner": "25205",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "502"
}, {
    "name": "Rinteln",
    "einwohner": "25187",
    "bundesland": "Niedersachsen",
    "rank": "503"
}, {
    "name": "Salzkotten",
    "einwohner": "25186",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "504"
}, {
    "name": "Lindau (Bodensee)",
    "einwohner": "25132",
    "bundesland": "Bayern",
    "rank": "505"
}, {
    "name": "Norden",
    "einwohner": "25117",
    "bundesland": "Niedersachsen",
    "rank": "506"
}, {
    "name": "Friedrichsdorf",
    "einwohner": "25092",
    "bundesland": "Hessen",
    "rank": "507"
}, {
    "name": "Saalfeld/Saale",
    "einwohner": "25041",
    "bundesland": "Thüringen",
    "rank": "508"
}, {
    "name": "Ludwigsfelde",
    "einwohner": "25030",
    "bundesland": "Brandenburg",
    "rank": "509"
}, {
    "name": "Achern",
    "einwohner": "25018",
    "bundesland": "Baden-Württemberg",
    "rank": "510"
}, {
    "name": "Bingen am Rhein",
    "einwohner": "24987",
    "bundesland": "Rheinland-Pfalz",
    "rank": "511"
}, {
    "name": "Bad Oldesloe",
    "einwohner": "24938",
    "bundesland": "Schleswig-Holstein",
    "rank": "512"
}, {
    "name": "Espelkamp",
    "einwohner": "24921",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "513"
}, {
    "name": "Rottweil",
    "einwohner": "24915",
    "bundesland": "Baden-Württemberg",
    "rank": "514"
}, {
    "name": "Werder (Havel)",
    "einwohner": "24856",
    "bundesland": "Brandenburg",
    "rank": "515"
}, {
    "name": "Delitzsch",
    "einwohner": "24850",
    "bundesland": "Sachsen",
    "rank": "516"
}, {
    "name": "Roth",
    "einwohner": "24819",
    "bundesland": "Bayern",
    "rank": "517"
}, {
    "name": "Harsewinkel",
    "einwohner": "24769",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "518"
}, {
    "name": "Olpe",
    "einwohner": "24757",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "519"
}, {
    "name": "Attendorn",
    "einwohner": "24676",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "520"
}, {
    "name": "Horb am Neckar",
    "einwohner": "24665",
    "bundesland": "Baden-Württemberg",
    "rank": "521"
}, {
    "name": "Groß-Gerau",
    "einwohner": "24648",
    "bundesland": "Hessen",
    "rank": "522"
}, {
    "name": "Ditzingen",
    "einwohner": "24633",
    "bundesland": "Baden-Württemberg",
    "rank": "523"
}, {
    "name": "Zirndorf",
    "einwohner": "24627",
    "bundesland": "Bayern",
    "rank": "524"
}, {
    "name": "Senftenberg",
    "einwohner": "24625",
    "bundesland": "Brandenburg",
    "rank": "525"
}, {
    "name": "Quedlinburg",
    "einwohner": "24555",
    "bundesland": "Sachsen-Anhalt",
    "rank": "526"
}, {
    "name": "Pfungstadt",
    "einwohner": "24548",
    "bundesland": "Hessen",
    "rank": "527"
}, {
    "name": "Arnstadt",
    "einwohner": "24481",
    "bundesland": "Thüringen",
    "rank": "528"
}, {
    "name": "Weingarten",
    "einwohner": "24460",
    "bundesland": "Baden-Württemberg",
    "rank": "529"
}, {
    "name": "Obertshausen",
    "einwohner": "24443",
    "bundesland": "Hessen",
    "rank": "530"
}, {
    "name": "Unterhaching",
    "einwohner": "24437",
    "bundesland": "Bayern",
    "rank": "531"
}, {
    "name": "Salzwedel",
    "einwohner": "24410",
    "bundesland": "Sachsen-Anhalt",
    "rank": "532"
}, {
    "name": "Geretsried",
    "einwohner": "24392",
    "bundesland": "Bayern",
    "rank": "533"
}, {
    "name": "Rathenow",
    "einwohner": "24387",
    "bundesland": "Brandenburg",
    "rank": "534"
}, {
    "name": "Übach-Palenberg",
    "einwohner": "24377",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "535"
}, {
    "name": "Meckenheim",
    "einwohner": "24357",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "536"
}, {
    "name": "Schleswig",
    "einwohner": "24266",
    "bundesland": "Schleswig-Holstein",
    "rank": "537"
}, {
    "name": "Lüdinghausen",
    "einwohner": "24263",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "538"
}, {
    "name": "Markkleeberg",
    "einwohner": "24240",
    "bundesland": "Sachsen",
    "rank": "539"
}, {
    "name": "Eisleben Lutherstadt",
    "einwohner": "24198",
    "bundesland": "Sachsen-Anhalt",
    "rank": "540"
}, {
    "name": "Idstein",
    "einwohner": "24108",
    "bundesland": "Hessen",
    "rank": "541"
}, {
    "name": "Hann Münden",
    "einwohner": "24072",
    "bundesland": "Niedersachsen",
    "rank": "542"
}, {
    "name": "Stutensee",
    "einwohner": "24063",
    "bundesland": "Baden-Württemberg",
    "rank": "543"
}, {
    "name": "Limbach-Oberfrohna",
    "einwohner": "24059",
    "bundesland": "Sachsen",
    "rank": "544"
}, {
    "name": "Syke",
    "einwohner": "24018",
    "bundesland": "Niedersachsen",
    "rank": "545"
}, {
    "name": "Varel",
    "einwohner": "23884",
    "bundesland": "Niedersachsen",
    "rank": "546"
}, {
    "name": "Isernhagen",
    "einwohner": "23792",
    "bundesland": "Niedersachsen",
    "rank": "547"
}, {
    "name": "Ronnenberg",
    "einwohner": "23752",
    "bundesland": "Niedersachsen",
    "rank": "548"
}, {
    "name": "Sonneberg",
    "einwohner": "23736",
    "bundesland": "Thüringen",
    "rank": "549"
}, {
    "name": "Waldshut-Tiengen",
    "einwohner": "23674",
    "bundesland": "Baden-Württemberg",
    "rank": "550"
}, {
    "name": "Warburg",
    "einwohner": "23629",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "551"
}, {
    "name": "Haren",
    "einwohner": "23545",
    "bundesland": "Niedersachsen",
    "rank": "552"
}, {
    "name": "Korbach",
    "einwohner": "23515",
    "bundesland": "Hessen",
    "rank": "553"
}, {
    "name": "Dillenburg",
    "einwohner": "23510",
    "bundesland": "Hessen",
    "rank": "554"
}, {
    "name": "Sehnde",
    "einwohner": "23489",
    "bundesland": "Niedersachsen",
    "rank": "555"
}, {
    "name": "Öhringen",
    "einwohner": "23489",
    "bundesland": "Baden-Württemberg",
    "rank": "556"
}, {
    "name": "Alfter",
    "einwohner": "23435",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "557"
}, {
    "name": "Wertheim",
    "einwohner": "23405",
    "bundesland": "Baden-Württemberg",
    "rank": "558"
}, {
    "name": "Netphen",
    "einwohner": "23393",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "559"
}, {
    "name": "Bedburg",
    "einwohner": "23334",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "560"
}, {
    "name": "Jüchen",
    "einwohner": "23260",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "561"
}, {
    "name": "Glauchau",
    "einwohner": "23255",
    "bundesland": "Sachsen",
    "rank": "562"
}, {
    "name": "Helmstedt",
    "einwohner": "23254",
    "bundesland": "Niedersachsen",
    "rank": "563"
}, {
    "name": "Calw",
    "einwohner": "23232",
    "bundesland": "Baden-Württemberg",
    "rank": "564"
}, {
    "name": "Walsrode",
    "einwohner": "23219",
    "bundesland": "Niedersachsen",
    "rank": "565"
}, {
    "name": "Gardelegen",
    "einwohner": "23148",
    "bundesland": "Sachsen-Anhalt",
    "rank": "566"
}, {
    "name": "Moormerland",
    "einwohner": "23138",
    "bundesland": "Niedersachsen",
    "rank": "567"
}, {
    "name": "Herzogenaurach",
    "einwohner": "23095",
    "bundesland": "Bayern",
    "rank": "568"
}, {
    "name": "Bad Mergentheim",
    "einwohner": "23064",
    "bundesland": "Baden-Württemberg",
    "rank": "569"
}, {
    "name": "Wallenhorst",
    "einwohner": "23018",
    "bundesland": "Niedersachsen",
    "rank": "570"
}, {
    "name": "Mosbach",
    "einwohner": "23000",
    "bundesland": "Baden-Württemberg",
    "rank": "571"
}, {
    "name": "Riedstadt30",
    "einwohner": "22998",
    "bundesland": "Hessen",
    "rank": "572"
}, {
    "name": "Burg",
    "einwohner": "22970",
    "bundesland": "Sachsen-Anhalt",
    "rank": "573"
}, {
    "name": "Starnberg",
    "einwohner": "22939",
    "bundesland": "Bayern",
    "rank": "574"
}, {
    "name": "Waldkraiburg",
    "einwohner": "22917",
    "bundesland": "Bayern",
    "rank": "575"
}, {
    "name": "Rudolstadt",
    "einwohner": "22855",
    "bundesland": "Thüringen",
    "rank": "576"
}, {
    "name": "Herdecke",
    "einwohner": "22818",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "577"
}, {
    "name": "Vaterstetten",
    "einwohner": "22768",
    "bundesland": "Bayern",
    "rank": "578"
}, {
    "name": "Vreden",
    "einwohner": "22688",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "579"
}, {
    "name": "Gelnhausen",
    "einwohner": "22687",
    "bundesland": "Hessen",
    "rank": "580"
}, {
    "name": "Freudenstadt",
    "einwohner": "22579",
    "bundesland": "Baden-Württemberg",
    "rank": "581"
}, {
    "name": "Lengerich",
    "einwohner": "22461",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "582"
}, {
    "name": "Husum",
    "einwohner": "22430",
    "bundesland": "Schleswig-Holstein",
    "rank": "583"
}, {
    "name": "Überlingen",
    "einwohner": "22408",
    "bundesland": "Baden-Württemberg",
    "rank": "584"
}, {
    "name": "Leutkirch im Allgäu",
    "einwohner": "22406",
    "bundesland": "Baden-Württemberg",
    "rank": "585"
}, {
    "name": "Radevormwald",
    "einwohner": "22386",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "586"
}, {
    "name": "Apolda",
    "einwohner": "22364",
    "bundesland": "Thüringen",
    "rank": "587"
}, {
    "name": "Schwetzingen",
    "einwohner": "22335",
    "bundesland": "Baden-Württemberg",
    "rank": "588"
}, {
    "name": "Spremberg",
    "einwohner": "22326",
    "bundesland": "Brandenburg",
    "rank": "589"
}, {
    "name": "Osterode am Harz",
    "einwohner": "22201",
    "bundesland": "Niedersachsen",
    "rank": "590"
}, {
    "name": "Weilheim in Oberbayern",
    "einwohner": "22184",
    "bundesland": "Bayern",
    "rank": "591"
}, {
    "name": "Karben",
    "einwohner": "22163",
    "bundesland": "Hessen",
    "rank": "592"
}, {
    "name": "Bad Soden am Taunus",
    "einwohner": "22161",
    "bundesland": "Hessen",
    "rank": "593"
}, {
    "name": "Westerstede",
    "einwohner": "22154",
    "bundesland": "Niedersachsen",
    "rank": "594"
}, {
    "name": "Wandlitz",
    "einwohner": "22095",
    "bundesland": "Brandenburg",
    "rank": "595"
}, {
    "name": "Zerbst/Anhalt",
    "einwohner": "22055",
    "bundesland": "Sachsen-Anhalt",
    "rank": "596"
}, {
    "name": "Sondershausen",
    "einwohner": "22039",
    "bundesland": "Thüringen",
    "rank": "597"
}, {
    "name": "Rastede",
    "einwohner": "22027",
    "bundesland": "Niedersachsen",
    "rank": "598"
}, {
    "name": "Friesoythe",
    "einwohner": "21918",
    "bundesland": "Niedersachsen",
    "rank": "599"
}, {
    "name": "Senden",
    "einwohner": "21909",
    "bundesland": "Bayern",
    "rank": "600"
}, {
    "name": "Gersthofen",
    "einwohner": "21908",
    "bundesland": "Bayern",
    "rank": "601"
}, {
    "name": "Stadtallendorf",
    "einwohner": "21861",
    "bundesland": "Hessen",
    "rank": "602"
}, {
    "name": "Eckernförde",
    "einwohner": "21859",
    "bundesland": "Schleswig-Holstein",
    "rank": "603"
}, {
    "name": "Edewecht",
    "einwohner": "21827",
    "bundesland": "Niedersachsen",
    "rank": "604"
}, {
    "name": "Stadthagen",
    "einwohner": "21814",
    "bundesland": "Niedersachsen",
    "rank": "605"
}, {
    "name": "Büdingen",
    "einwohner": "21785",
    "bundesland": "Hessen",
    "rank": "606"
}, {
    "name": "Büren",
    "einwohner": "21772",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "607"
}, {
    "name": "Donaueschingen",
    "einwohner": "21746",
    "bundesland": "Baden-Württemberg",
    "rank": "608"
}, {
    "name": "Bad Harzburg",
    "einwohner": "21735",
    "bundesland": "Niedersachsen",
    "rank": "609"
}, {
    "name": "Halle (Westf)",
    "einwohner": "21709",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "610"
}, {
    "name": "Bad Kissingen",
    "einwohner": "21696",
    "bundesland": "Bayern",
    "rank": "611"
}, {
    "name": "Nagold",
    "einwohner": "21687",
    "bundesland": "Baden-Württemberg",
    "rank": "612"
}, {
    "name": "Neusäß",
    "einwohner": "21617",
    "bundesland": "Bayern",
    "rank": "613"
}, {
    "name": "Ilsede(*)",
    "einwohner": "21617",
    "bundesland": "Niedersachsen",
    "rank": "614"
}, {
    "name": "Metzingen",
    "einwohner": "21612",
    "bundesland": "Baden-Württemberg",
    "rank": "615"
}, {
    "name": "Waldkirch",
    "einwohner": "21561",
    "bundesland": "Baden-Württemberg",
    "rank": "616"
}, {
    "name": "Döbeln",
    "einwohner": "21516",
    "bundesland": "Sachsen",
    "rank": "617"
}, {
    "name": "Xanten",
    "einwohner": "21510",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "618"
}, {
    "name": "Wipperfürth",
    "einwohner": "21481",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "619"
}, {
    "name": "Heide",
    "einwohner": "21422",
    "bundesland": "Schleswig-Holstein",
    "rank": "620"
}, {
    "name": "Soltau",
    "einwohner": "21414",
    "bundesland": "Niedersachsen",
    "rank": "621"
}, {
    "name": "Rotenburg (Wümme)",
    "einwohner": "21392",
    "bundesland": "Niedersachsen",
    "rank": "622"
}, {
    "name": "Lindlar(*)",
    "einwohner": "21382",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "623"
}, {
    "name": "Rees",
    "einwohner": "21349",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "624"
}, {
    "name": "Eppingen",
    "einwohner": "21312",
    "bundesland": "Baden-Württemberg",
    "rank": "625"
}, {
    "name": "Sonthofen",
    "einwohner": "21300",
    "bundesland": "Bayern",
    "rank": "626"
}, {
    "name": "Ottobrunn(*)",
    "einwohner": "21295",
    "bundesland": "Bayern",
    "rank": "627"
}, {
    "name": "Elsdorf",
    "einwohner": "21232",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "628"
}, {
    "name": "Meiningen",
    "einwohner": "21231",
    "bundesland": "Thüringen",
    "rank": "629"
}, {
    "name": "Versmold",
    "einwohner": "21230",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "630"
}, {
    "name": "Wülfrath",
    "einwohner": "21223",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "631"
}, {
    "name": "Waren (Müritz)",
    "einwohner": "21153",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "632"
}, {
    "name": "Laupheim",
    "einwohner": "21153",
    "bundesland": "Baden-Württemberg",
    "rank": "633"
}, {
    "name": "Hockenheim",
    "einwohner": "21130",
    "bundesland": "Baden-Württemberg",
    "rank": "634"
}, {
    "name": "Puchheim",
    "einwohner": "21111",
    "bundesland": "Bayern",
    "rank": "635"
}, {
    "name": "Duderstadt",
    "einwohner": "21072",
    "bundesland": "Niedersachsen",
    "rank": "636"
}, {
    "name": "Geseke",
    "einwohner": "21070",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "637"
}, {
    "name": "Aichach",
    "einwohner": "21050",
    "bundesland": "Bayern",
    "rank": "638"
}, {
    "name": "Greiz",
    "einwohner": "21042",
    "bundesland": "Thüringen",
    "rank": "639"
}, {
    "name": "Schwanewede(*)",
    "einwohner": "21040",
    "bundesland": "Niedersachsen",
    "rank": "640"
}, {
    "name": "Werdau",
    "einwohner": "21039",
    "bundesland": "Sachsen",
    "rank": "641"
}, {
    "name": "Blieskastel",
    "einwohner": "21033",
    "bundesland": "Saarland",
    "rank": "642"
}, {
    "name": "Schramberg",
    "einwohner": "20985",
    "bundesland": "Baden-Württemberg",
    "rank": "643"
}, {
    "name": "Seligenstadt",
    "einwohner": "20980",
    "bundesland": "Hessen",
    "rank": "644"
}, {
    "name": "Fröndenberg/Ruhr",
    "einwohner": "20961",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "645"
}, {
    "name": "Coswig",
    "einwohner": "20831",
    "bundesland": "Sachsen",
    "rank": "646"
}, {
    "name": "Neu Wulmstorf(*)",
    "einwohner": "20828",
    "bundesland": "Niedersachsen",
    "rank": "647"
}, {
    "name": "Eschborn",
    "einwohner": "20824",
    "bundesland": "Hessen",
    "rank": "648"
}, {
    "name": "Groß-Umstadt",
    "einwohner": "20821",
    "bundesland": "Hessen",
    "rank": "649"
}, {
    "name": "Herborn",
    "einwohner": "20816",
    "bundesland": "Hessen",
    "rank": "650"
}, {
    "name": "Traunreut",
    "einwohner": "20799",
    "bundesland": "Bayern",
    "rank": "651"
}, {
    "name": "Kitzingen",
    "einwohner": "20756",
    "bundesland": "Bayern",
    "rank": "652"
}, {
    "name": "Steinhagen(*)",
    "einwohner": "20749",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "653"
}, {
    "name": "Wittmund",
    "einwohner": "20735",
    "bundesland": "Niedersachsen",
    "rank": "654"
}, {
    "name": "Meinerzhagen",
    "einwohner": "20670",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "655"
}, {
    "name": "Enger",
    "einwohner": "20658",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "656"
}, {
    "name": "Kleinmachnow(*)",
    "einwohner": "20655",
    "bundesland": "Brandenburg",
    "rank": "657"
}, {
    "name": "Burgwedel32",
    "einwohner": "20654",
    "bundesland": "Niedersachsen",
    "rank": "658"
}, {
    "name": "Waghäusel",
    "einwohner": "20629",
    "bundesland": "Baden-Württemberg",
    "rank": "659"
}, {
    "name": "Flörsheim am Main",
    "einwohner": "20623",
    "bundesland": "Hessen",
    "rank": "660"
}, {
    "name": "Quickborn",
    "einwohner": "20608",
    "bundesland": "Schleswig-Holstein",
    "rank": "661"
}, {
    "name": "Germersheim",
    "einwohner": "20587",
    "bundesland": "Rheinland-Pfalz",
    "rank": "662"
}, {
    "name": "Haar(*)",
    "einwohner": "20513",
    "bundesland": "Bayern",
    "rank": "663"
}, {
    "name": "Wilnsdorf(*)",
    "einwohner": "20512",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "664"
}, {
    "name": "Bad Rappenau",
    "einwohner": "20510",
    "bundesland": "Baden-Württemberg",
    "rank": "665"
}, {
    "name": "Neustrelitz",
    "einwohner": "20504",
    "bundesland": "Mecklenburg-Vorpommern",
    "rank": "666"
}, {
    "name": "Westoverledingen(*)",
    "einwohner": "20482",
    "bundesland": "Niedersachsen",
    "rank": "667"
}, {
    "name": "Wachtberg(*)",
    "einwohner": "20457",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "668"
}, {
    "name": "Senden(*)",
    "einwohner": "20455",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "669"
}, {
    "name": "Annaberg-Buchholz",
    "einwohner": "20426",
    "bundesland": "Sachsen",
    "rank": "670"
}, {
    "name": "Bruchköbel",
    "einwohner": "20418",
    "bundesland": "Hessen",
    "rank": "671"
}, {
    "name": "Stadtlohn",
    "einwohner": "20411",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "672"
}, {
    "name": "Luckenwalde",
    "einwohner": "20358",
    "bundesland": "Brandenburg",
    "rank": "673"
}, {
    "name": "Kaltenkirchen",
    "einwohner": "20331",
    "bundesland": "Schleswig-Holstein",
    "rank": "674"
}, {
    "name": "Rheinstetten33",
    "einwohner": "20330",
    "bundesland": "Baden-Württemberg",
    "rank": "675"
}, {
    "name": "Schortens",
    "einwohner": "20321",
    "bundesland": "Niedersachsen",
    "rank": "676"
}, {
    "name": "Dillingen/Saar",
    "einwohner": "20311",
    "bundesland": "Saarland",
    "rank": "677"
}, {
    "name": "Blankenburg (Harz)",
    "einwohner": "20294",
    "bundesland": "Sachsen-Anhalt",
    "rank": "678"
}, {
    "name": "Gauting(*)",
    "einwohner": "20268",
    "bundesland": "Bayern",
    "rank": "679"
}, {
    "name": "Haßloch(*)",
    "einwohner": "20254",
    "bundesland": "Rheinland-Pfalz",
    "rank": "680"
}, {
    "name": "Eislingen/Fils",
    "einwohner": "20177",
    "bundesland": "Baden-Württemberg",
    "rank": "681"
}, {
    "name": "Lichtenfels",
    "einwohner": "20169",
    "bundesland": "Bayern",
    "rank": "682"
}, {
    "name": "Panketal(*)",
    "einwohner": "20131",
    "bundesland": "Brandenburg",
    "rank": "683"
}, {
    "name": "Uetze(*)",
    "einwohner": "20103",
    "bundesland": "Niedersachsen",
    "rank": "684"
}, {
    "name": "Holzminden",
    "einwohner": "20099",
    "bundesland": "Niedersachsen",
    "rank": "685"
}, {
    "name": "Karlsfeld(*)",
    "einwohner": "20096",
    "bundesland": "Bayern",
    "rank": "686"
}, {
    "name": "Zülpich",
    "einwohner": "20091",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "687"
}, {
    "name": "Torgau",
    "einwohner": "20047",
    "bundesland": "Sachsen",
    "rank": "688"
}, {
    "name": "Günzburg",
    "einwohner": "20038",
    "bundesland": "Bayern",
    "rank": "689"
}, {
    "name": "Ennigerloh",
    "einwohner": "20037",
    "bundesland": "Nordrhein-Westfalen",
    "rank": "690"
}, {
    "name": "Bad Waldsee",
    "einwohner": "20011",
    "bundesland": "Baden-Württemberg",
    "rank": "691"
}];

var unis_parsed = [];
function getCity() {
  for (var i = 0; i< unis.length; i++) {
      for (var j = 0; j < cities.length; j++) {
        if (unis[i].name.indexOf(cities[j].name) >= 0) {
            unis_parsed[i] = {
                "name": unis[i].name,
                "bundesland": unis[i].bundesland,
                "stadt": cities[j].name
            }
            console.log("FOUND "+unis[i].name+" in "+cities[j].name);
        }
      }
  }
  $('body').html(JSON.stringify(unis_parsed));
}

unis = [{
    "name": "FH Aachen (FHA)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "RWTH Aachen (RWTH)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Aalen (HAA)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Albstadt-Sigmaringen (HAS)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Alanus Hochschule für Kunst und Gesellschaft (AHKG) (Alfter)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Allensbach Hochschule (ABHS)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Fachhochschule für Verwaltung und Dienstleistung (FVD) (Altenholz, Reinfeld)",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Ostbayerische Technische Hochschule Amberg-Weiden (OTH Amberg-Weiden)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Ansbach (HAH)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Aschaffenburg (HAB)",
    "bundesland": "Bayern"
}, {
    "name": "Fachhochschule Polizei Sachsen-Anhalt (FPS) (Aschersleben)",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Hochschule Augsburg (HSA)",
    "bundesland": "Bayern"
}, {
    "name": "Universität Augsburg (UAB)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule der Bayerischen Wirtschaft (HDBW)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule der Deutschen Gesetzlichen Unfallversicherung (HGU) (Bad Hersfeld)",
    "bundesland": "Hessen"
}, {
    "name": "accadis Hochschule Bad Homburg (AHBH)",
    "bundesland": "Hessen"
}, {
    "name": "Internationale Hochschule Bad Honnef Bonn (IHBH)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule für Rechtspflege Nordrhein-Westfalen (FFR) (Bad Münstereifel)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Diploma Hochschule (DPHS) (Bad Sooden-Allendorf)",
    "bundesland": "Hessen"
}, {
    "name": "Otto-Friedrich-Universität Bamberg (OFU)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für evangelische Kirchenmusik Bayreuth (HEKM)",
    "bundesland": "Bayern"
}, {
    "name": "Universität Bayreuth (UBT)",
    "bundesland": "Bayern"
}, {
    "name": "Akkon-Hochschule für Humanwissenschaften (AHFH) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Alice Salomon Hochschule Berlin (ASH)",
    "bundesland": "Berlin"
}, {
    "name": "bbw Hochschule (BBWH) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "BTK - Hochschule für Gestaltung (BTKB)",
    "bundesland": "Berlin"
}, {
    "name": "BEST-Sabel-Hochschule Berlin (BSHB)",
    "bundesland": "Berlin"
}, {
    "name": "BSP Business School Berlin (BBSB)",
    "bundesland": "Berlin"
}, {
    "name": "Beuth Hochschule für Technik Berlin",
    "bundesland": "Berlin"
}, {
    "name": "Dekra Hochschule Berlin (DHSB)",
    "bundesland": "Berlin"
}, {
    "name": "design akademie berlin, Hochschule für Kommunikation und Design (FHDA) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Deutsche Film- und Fernsehakademie Berlin (DFF)",
    "bundesland": "Berlin"
}, {
    "name": "Deutsche Universität für Weiterbildung (DUFW)",
    "bundesland": "Berlin"
}, {
    "name": "ECLA of Bard, a Liberal Arts University in Berlin (ECLA)",
    "bundesland": "Berlin"
}, {
    "name": "ESCP Europe Wirtschaftshochschule Berlin",
    "bundesland": "Berlin"
}, {
    "name": "ESMOD Internationale Kunsthochschule für Mode (IKHM) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "ESMT European School of Management and Technology (ESMT) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Evangelische Hochschule Berlin (EHSB)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule der populären Künste (FHPK)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Medien, Kommunikation und Wirtschaft (HMKW) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Technik und Wirtschaft Berlin (HTW)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Wirtschaft, Technik und Kultur Berlin (HWTK)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Wirtschaft und Recht Berlin (HWR)",
    "bundesland": "Berlin"
}, {
    "name": "Freie Universität Berlin (FUB)",
    "bundesland": "Berlin"
}, {
    "name": "German open Business School (GOBS) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Hertie School of Governance (HSOG) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "H:G Hochschule für Gesundheit und Sport (HSGS) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Musik „Hanns Eisler“ Berlin (HMB)",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule für Schauspielkunst „Ernst Busch“ Berlin (HSB)",
    "bundesland": "Berlin"
}, {
    "name": "Humboldt-Universität zu Berlin (HUB)",
    "bundesland": "Berlin"
}, {
    "name": "IB-Hochschule Berlin (IBHB)",
    "bundesland": "Berlin"
}, {
    "name": "International Psychoanalytic University Berlin (IPUB)",
    "bundesland": "Berlin"
}, {
    "name": "Katholische Hochschule für Sozialwesen Berlin (KHSW)",
    "bundesland": "Berlin"
}, {
    "name": "Kunsthochschule Berlin-Weißensee (KHB)",
    "bundesland": "Berlin"
}, {
    "name": "Mediadesign Hochschule (MDHB) (Berlin)",
    "bundesland": "Berlin"
}, {
    "name": "Psychologische Hochschule Berlin (PHSB)",
    "bundesland": "Berlin"
}, {
    "name": "SRH Hochschule Berlin (SRHB)",
    "bundesland": "Berlin"
}, {
    "name": "Steinbeis-Hochschule Berlin (SBHB)",
    "bundesland": "Berlin"
}, {
    "name": "Technische Universität Berlin (TUB)",
    "bundesland": "Berlin"
}, {
    "name": "Touro College Berlin",
    "bundesland": "Berlin"
}, {
    "name": "Universität der Künste Berlin",
    "bundesland": "Berlin"
}, {
    "name": "Quadriga Hochschule Berlin",
    "bundesland": "Berlin"
}, {
    "name": "Hochschule Anhalt (Bernburg, Dessau und Köthen)",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Hochschule Biberach",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Fachhochschule Bielefeld",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule der Diakonie (Bielefeld)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule des Mittelstands (Bielefeld)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Universität Bielefeld",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Bingen",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "EBZ Business School (Bochum)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Evangelische Fachhochschule Rheinland-Westfalen-Lippe (Bochum)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Bochum",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule für Gesundheit (Bochum)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Ruhr-Universität Bochum",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Technische Fachhochschule Georg Agricola (Bochum)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule der Sparkassen-Finanzgruppe (Bonn)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Rheinische Friedrich-Wilhelms-Universität Bonn",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Bonn-Rhein-Sieg",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Technische Hochschule Brandenburg",
    "bundesland": "Brandenburg"
}, {
    "name": "Medizinische Hochschule Brandenburg Theodor Fontane",
    "bundesland": "Brandenburg"
}, {
    "name": "Hochschule für Bildende Künste Braunschweig",
    "bundesland": "Niedersachsen"
}, {
    "name": "Technische Universität Braunschweig",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Bremen",
    "bundesland": "Bremen"
}, {
    "name": "Apollon Hochschule der Gesundheitswirtschaft (Bremen)",
    "bundesland": "Bremen"
}, {
    "name": "Hochschule für internationale Wirtschaft und Logistik (Bremen)",
    "bundesland": "Bremen"
}, {
    "name": "Hochschule für Künste Bremen",
    "bundesland": "Bremen"
}, {
    "name": "Hochschule für Öffentliche Verwaltung Bremen",
    "bundesland": "Bremen"
}, {
    "name": "Jacobs University Bremen",
    "bundesland": "Bremen"
}, {
    "name": "Universität Bremen",
    "bundesland": "Bremen"
}, {
    "name": "Hochschule Bremerhaven",
    "bundesland": "Bremen"
}, {
    "name": "Europäische Fachhochschule (Brühl)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule des Bundes für öffentliche Verwaltung (Brühl)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule 21 (Buxtehude)",
    "bundesland": "Niedersachsen"
}, {
    "name": "SRH Hochschule für Wirtschaft und Medien Calw",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Technische Universität Chemnitz",
    "bundesland": "Sachsen"
}, {
    "name": "Technische Universität Clausthal",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule für angewandte Wissenschaften Coburg",
    "bundesland": "Bayern"
}, {
    "name": "Brandenburgische Technische Universität Cottbus-Senftenberg",
    "bundesland": "Brandenburg"
}, {
    "name": "Evangelische Hochschule Darmstadt",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule Darmstadt",
    "bundesland": "Hessen"
}, {
    "name": "Technische Universität Darmstadt",
    "bundesland": "Hessen"
}, {
    "name": "Technische Hochschule Deggendorf",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Musik Detmold",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Dortmund",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "International School of Management (Dortmund)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Technische Universität Dortmund",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Dresden International University",
    "bundesland": "Sachsen"
}, {
    "name": "Evangelische Hochschule für Kirchenmusik Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Evangelische Hochschule für Soziale Arbeit Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Fachhochschule Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Bildende Künste Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Musik Carl Maria von Weber Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Technik und Wirtschaft Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Palucca Hochschule für Tanz Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Technische Universität Dresden",
    "bundesland": "Sachsen"
}, {
    "name": "Universität Duisburg-Essen",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Düsseldorf",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fliedner Fachhochschule Düsseldorf",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Heinrich-Heine-Universität Düsseldorf",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "IST-Hochschule für Management (Düsseldorf)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Kunstakademie Düsseldorf",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Robert-Schumann-Hochschule Düsseldorf",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule für nachhaltige Entwicklung Eberswalde",
    "bundesland": "Brandenburg"
}, {
    "name": "Fachhochschule für Finanzen (Edenkoben)",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Katholische Universität Eichstätt-Ingolstadt",
    "bundesland": "Bayern"
}, {
    "name": "Nordakademie (Elmshorn)",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Theologische Hochschule Elstal",
    "bundesland": "Brandenburg"
}, {
    "name": "Hochschule Emden/Leer",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule für angewandtes Management (Erding)",
    "bundesland": "Bayern"
}, {
    "name": "Adam-Ries-Fachhochschule (Erfurt)",
    "bundesland": "Thüringen"
}, {
    "name": "Fachhochschule Erfurt",
    "bundesland": "Thüringen"
}, {
    "name": "Universität Erfurt",
    "bundesland": "Thüringen"
}, {
    "name": "Friedrich-Alexander-Universität Erlangen-Nürnberg",
    "bundesland": "Bayern"
}, {
    "name": "Folkwang Universität der Künste (Essen)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "FOM Hochschule (Essen)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Esslingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Theologische Hochschule Ewersbach",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule Flensburg (Fachhochschule)",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Europa-Universität Flensburg",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Fachhochschule Frankfurt am Main",
    "bundesland": "Hessen"
}, {
    "name": "Frankfurt School of Finance & Management (Frankfurt am Main)",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule für Musik und Darstellende Kunst Frankfurt am Main",
    "bundesland": "Hessen"
}, {
    "name": "Johann Wolfgang Goethe-Universität Frankfurt am Main",
    "bundesland": "Hessen"
}, {
    "name": "Philosophisch-Theologische Hochschule Sankt Georgen (Frankfurt am Main)",
    "bundesland": "Hessen"
}, {
    "name": "Provadis School of International Management and Technology (Frankfurt am Main)",
    "bundesland": "Hessen"
}, {
    "name": "Staatliche Hochschule für Bildende Künste – Städelschule (Frankfurt am Main)",
    "bundesland": "Hessen"
}, {
    "name": "Europa-Universität Viadrina (Frankfurt (Oder))",
    "bundesland": "Brandenburg"
}, {
    "name": "Technische Universität Bergakademie Freiberg",
    "bundesland": "Sachsen"
}, {
    "name": "Albert-Ludwigs-Universität Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Evangelische Hochschule Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Musik Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Kunst, Design und Populäre Musik Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Katholische Hochschule Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Pädagogische Hochschule Freiburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Theologische Hochschule Friedensau",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Zeppelin Universität (Friedrichshafen)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Fulda",
    "bundesland": "Hessen"
}, {
    "name": "Theologische Fakultät Fulda",
    "bundesland": "Hessen"
}, {
    "name": "Wilhelm Löhe Hochschule für angewandte Wissenschaften (Fürth)",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Furtwangen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Geisenheim",
    "bundesland": "Hessen"
}, {
    "name": "Fachhochschule für öffentliche Verwaltung Nordrhein-Westfalen (Gelsenkirchen)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Westfälische Hochschule Gelsenkirchen Bocholt Recklinghausen",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "SRH Hochschule für Gesundheit Gera",
    "bundesland": "Thüringen"
}, {
    "name": "Freie Theologische Hochschule Gießen",
    "bundesland": "Hessen"
}, {
    "name": "Justus-Liebig-Universität Gießen",
    "bundesland": "Hessen"
}, {
    "name": "Technische Hochschule Mittelhessen (Gießen, Friedberg, Wetzlar)",
    "bundesland": "Hessen"
}, {
    "name": "Thüringer Fachhochschule für öffentliche Verwaltung (Gotha, Meiningen)",
    "bundesland": "Thüringen"
}, {
    "name": "Georg-August-Universität Göttingen",
    "bundesland": "Niedersachsen"
}, {
    "name": "Private Fachhochschule Göttingen",
    "bundesland": "Niedersachsen"
}, {
    "name": "Ernst-Moritz-Arndt-Universität Greifswald",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Fachhochschule für öffentliche Verwaltung, Polizei und Rechtspflege (Güstrow)",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Hochschule der Deutschen Bundesbank (Hachenburg)",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Fernuniversität in Hagen",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Burg Giebichenstein Kunsthochschule Halle",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Evangelische Hochschule für Kirchenmusik Halle",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Martin-Luther-Universität Halle-Wittenberg",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Bucerius Law School (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "Brand Academy – Hochschule für Design und Kommunikation (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "EBC Hochschule",
    "bundesland": "Hamburg"
}, {
    "name": "Europäische Fernhochschule Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Evangelische Hochschule für Soziale Arbeit & Diakonie (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "HafenCity Universität Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "HFH Hamburger Fern-Hochschule",
    "bundesland": "Hamburg"
}, {
    "name": "Hamburg School of Business Administration",
    "bundesland": "Hamburg"
}, {
    "name": "Helmut-Schmidt-Universität (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "Hochschule für Angewandte Wissenschaften Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Hochschule für bildende Künste Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Hochschule für Musik und Theater Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Norddeutsche Akademie für Finanzen und Steuerrecht (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "International Business School of Service Management (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "Kühne Logistics University (Hamburg)",
    "bundesland": "Hamburg"
}, {
    "name": "MSH Medical School Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Technische Universität Hamburg-Harburg",
    "bundesland": "Hamburg"
}, {
    "name": "Universität Hamburg",
    "bundesland": "Hamburg"
}, {
    "name": "Hochschule Weserbergland (Hameln)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Hamm-Lippstadt",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "SRH Hochschule für Logistik und Wirtschaft Hamm",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule für die Wirtschaft (Hannover)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Gottfried Wilhelm Leibniz Universität Hannover",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule für Musik, Theater und Medien Hannover",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Hannover",
    "bundesland": "Niedersachsen"
}, {
    "name": "Kommunale Hochschule für Verwaltung in Niedersachsen (Hannover)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Leibniz-Akademie Hannover (Hannover)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Medizinische Hochschule Hannover",
    "bundesland": "Niedersachsen"
}, {
    "name": "Tierärztliche Hochschule Hannover",
    "bundesland": "Niedersachsen"
}, {
    "name": "Fachhochschule Westküste (Heide)",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Hochschule für Jüdische Studien (Heidelberg)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Kirchenmusik Heidelberg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Pädagogische Hochschule Heidelberg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Ruprecht-Karls-Universität Heidelberg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "SRH Hochschule Heidelberg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Heilbronn",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "German Graduate School of Management and Law (Heilbronn)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Kirchenmusik der Evangelischen Kirche von Westfalen (Herford)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule für Interkulturelle Theologie (Hermannsburg)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Norddeutsche Hochschule für Rechtspflege (Hildesheim)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Universität Hildesheim",
    "bundesland": "Niedersachsen"
}, {
    "name": "HAWK Hochschule Hildesheim/Holzminden/Göttingen",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Hof",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Fresenius (Idstein)",
    "bundesland": "Hessen"
}, {
    "name": "Technische Universität Ilmenau",
    "bundesland": "Thüringen"
}, {
    "name": "Technische Hochschule Ingolstadt",
    "bundesland": "Bayern"
}, {
    "name": "Business and Information Technology School (Iserlohn, Hamburg, Berlin)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Südwestfalen (Iserlohn)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Naturwissenschaftlich-Technische Akademie Isny",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Ernst-Abbe-Fachhochschule Jena",
    "bundesland": "Thüringen"
}, {
    "name": "Friedrich-Schiller-Universität Jena",
    "bundesland": "Thüringen"
}, {
    "name": "Hochschule Kaiserslautern",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Technische Universität Kaiserslautern",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Hochschule für Musik Karlsruhe",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Karlsruhe – Technik und Wirtschaft",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Karlshochschule International University (Karlsruhe)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Karlsruher Institut für Technologie",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Pädagogische Hochschule Karlsruhe",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Staatliche Akademie der Bildenden Künste Karlsruhe",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Staatliche Hochschule für Gestaltung Karlsruhe",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "CVJM-Hochschule (Kassel)",
    "bundesland": "Hessen"
}, {
    "name": "Kunsthochschule Kassel",
    "bundesland": "Hessen"
}, {
    "name": "Universität Kassel",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule für öffentliche Verwaltung Kehl",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für angewandte Wissenschaften Kempten",
    "bundesland": "Bayern"
}, {
    "name": "Christian-Albrechts-Universität zu Kiel",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Fachhochschule Kiel",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Muthesius Kunsthochschule (Kiel)",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Hochschule Rhein-Waal (Kleve, Kamp-Lintfort)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Koblenz",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Universität Koblenz-Landau",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Hochschule Konstanz Technik, Wirtschaft und Gestaltung",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Konstanz",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Cologne Business School (Köln)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Deutsche Sporthochschule Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Technische Hochschule Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule für Musik und Tanz Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Katholische Hochschule Nordrhein-Westfalen (Köln, Münster, Paderborn, Aachen)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Kunsthochschule für Medien Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Universität zu Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Rheinische Fachhochschule Köln",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule für Finanzen Brandenburg (Königs Wusterhausen)",
    "bundesland": "Brandenburg"
}, {
    "name": "Wissenschaftliche Hochschule Lahr",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Landshut",
    "bundesland": "Bayern"
}, {
    "name": "Handelshochschule Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Grafik und Buchkunst Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Musik und Theater „Felix Mendelssohn Bartholdy“ Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Technik, Wirtschaft und Kultur Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Telekommunikation Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Universität Leipzig",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule Ostwestfalen-Lippe (Lemgo)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Internationale Hochschule Liebenzell",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Akademie für Darstellende Kunst Baden-Württemberg (Ludwigsburg)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Filmakademie Baden-Württemberg (Ludwigsburg)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Evangelische Hochschule Ludwigsburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für öffentliche Verwaltung und Finanzen Ludwigsburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Pädagogische Hochschule Ludwigsburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Ludwigshafen am Rhein",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Fachhochschule Lübeck",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Leuphana Universität Lüneburg",
    "bundesland": "Niedersachsen"
}, {
    "name": "Musikhochschule Lübeck",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Universität zu Lübeck",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Otto-von-Guericke-Universität Magdeburg",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Hochschule Magdeburg-Stendal",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Hochschule Mainz",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Katholische Hochschule Mainz",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Johannes Gutenberg-Universität Mainz",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Hochschule der Bundesagentur für Arbeit (Mannheim, Schwerin)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule der Wirtschaft für Management Mannheim",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Musik und Darstellende Kunst Mannheim",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Mannheim",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Popakademie Baden-Württemberg (Mannheim)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Mannheim",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Archivschule Marburg",
    "bundesland": "Hessen"
}, {
    "name": "Evangelische Hochschule Tabor (Marburg)",
    "bundesland": "Hessen"
}, {
    "name": "Philipps-Universität Marburg",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule für öffentliche Verwaltung Rheinland-Pfalz (Mayen, Lautzenhausen)",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Fachhochschule für öffentliche Verwaltung und Rechtspflege Meißen",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule Merseburg",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "Hochschule Mittweida",
    "bundesland": "Sachsen"
}, {
    "name": "Evangelische Hochschule Moritzburg",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule Ruhr West (Mülheim, Bottrop)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Akademie der Bildenden Künste München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Angewandte Sprachen München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für angewandte Wissenschaften München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Fernsehen und Film München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Musik und Theater München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Philosophie München",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Politik München",
    "bundesland": "Bayern"
}, {
    "name": "Fachhochschule für öffentliche Verwaltung und Rechtspflege in Bayern (München)",
    "bundesland": "Bayern"
}, {
    "name": "Katholische Stiftungsfachhochschule München",
    "bundesland": "Bayern"
}, {
    "name": "Ludwig-Maximilians-Universität München",
    "bundesland": "Bayern"
}, {
    "name": "Macromedia Hochschule für Medien und Kommunikation (München)",
    "bundesland": "Bayern"
}, {
    "name": "Munich Business School (München)",
    "bundesland": "Bayern"
}, {
    "name": "Technische Universität München",
    "bundesland": "Bayern"
}, {
    "name": "Ukrainische Freie Universität München",
    "bundesland": "Bayern"
}, {
    "name": "Universität der Bundeswehr München",
    "bundesland": "Bayern"
}, {
    "name": "Deutsche Hochschule der Polizei (Münster)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Münster",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Kunstakademie Münster",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Philosophisch-Theologische Hochschule Münster",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Westfälische Wilhelms-Universität (Münster)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule für angewandte Wissenschaften Neu-Ulm",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Neubrandenburg",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Augustana-Hochschule Neuendettelsau",
    "bundesland": "Bayern"
}, {
    "name": "Rheinische Fachhochschule Neuss für Internationale Wirtschaft",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule Niederrhein (Krefeld/Mönchengladbach)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Nordhausen",
    "bundesland": "Thüringen"
}, {
    "name": "Fachhochschule für Finanzen Nordrhein-Westfalen (Nordkirchen)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Akademie der Bildenden Künste Nürnberg",
    "bundesland": "Bayern"
}, {
    "name": "Evangelische Hochschule Nürnberg",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Musik Nürnberg",
    "bundesland": "Bayern"
}, {
    "name": "Technische Hochschule Nürnberg Georg Simon Ohm",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Kunsttherapie Nürtingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Wirtschaft und Umwelt Nürtingen-Geislingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Lutherische Theologische Hochschule Oberursel",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule für Gestaltung Offenbach am Main",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule Offenburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Carl von Ossietzky Universität Oldenburg",
    "bundesland": "Niedersachsen"
}, {
    "name": "Fachhochschule der Polizei des Landes Brandenburg (Oranienburg)",
    "bundesland": "Brandenburg"
}, {
    "name": "Hochschule Osnabrück",
    "bundesland": "Niedersachsen"
}, {
    "name": "Universität Osnabrück",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule für Künste im Sozialen Ottersberg",
    "bundesland": "Niedersachsen"
}, {
    "name": "Fachhochschule der Wirtschaft (Paderborn)",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Theologische Fakultät Paderborn",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Universität Paderborn",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Universität Passau",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Pforzheim",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Fachhochschule für Sport & Management Potsdam",
    "bundesland": "Brandenburg"
}, {
    "name": "Fachhochschule Potsdam",
    "bundesland": "Brandenburg"
}, {
    "name": "Filmuniversität Babelsberg (Potsdam)",
    "bundesland": "Brandenburg"
}, {
    "name": "Universität Potsdam",
    "bundesland": "Brandenburg"
}, {
    "name": "Hochschule Ravensburg-Weingarten",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Katholische Kirchenmusik und Musikpädagogik Regensburg",
    "bundesland": "Bayern"
}, {
    "name": "Universität Regensburg",
    "bundesland": "Bayern"
}, {
    "name": "Ostbayerische Technische Hochschule Regensburg",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Reutlingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Theologische Hochschule Reutlingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Mathias Hochschule Rheine",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "SRH Fernhochschule Riedlingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Rosenheim",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Musik und Theater Rostock",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Universität Rostock",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Hessische Hochschule für Finanzen und Rechtspflege (Rotenburg an der Fulda)",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule der Sächsischen Polizei (FH) (Rothenburg/Oberlausitz)",
    "bundesland": "Sachsen"
}, {
    "name": "Hochschule für Forstwirtschaft Rottenburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Katholische Hochschule für Kirchenmusik Rottenburg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Deutsche Hochschule für Prävention und Gesundheitsmanagement (Saarbrücken)",
    "bundesland": "Saarland"
}, {
    "name": "Fachhochschule für Verwaltung des Saarlandes (Saarbrücken)",
    "bundesland": "Saarland"
}, {
    "name": "Hochschule der Bildenden Künste Saar (Saarbrücken)",
    "bundesland": "Saarland"
}, {
    "name": "Hochschule für Musik Saar (Saarbrücken)",
    "bundesland": "Saarland"
}, {
    "name": "Hochschule für Technik und Wirtschaft des Saarlandes (Saarbrücken)",
    "bundesland": "Saarland"
}, {
    "name": "Universität des Saarlandes (Saarbrücken, Homburg)",
    "bundesland": "Saarland"
}, {
    "name": "Fachhochschule Schmalkalden",
    "bundesland": "Thüringen"
}, {
    "name": "Hochschule für Gestaltung Schwäbisch Gmünd",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Fachhochschule Schwetzingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Pädagogische Hochschule Schwäbisch Gmünd",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Siegen",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Deutsche Universität für Verwaltungswissenschaften Speyer",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Philosophisch-Theologische Hochschule SVD St. Augustin",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Fachhochschule Stralsund",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "AKAD Bildungsgesellschaft (Stuttgart)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Duale Hochschule Baden-Württemberg (Stuttgart)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Freie Hochschule Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule der Medien (Stuttgart)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Musik und Darstellende Kunst Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule für Technik Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Merz Akademie (Stuttgart)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Staatliche Akademie der Bildenden Künste Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "VWA Hochschule für berufsbegleitendes Studium Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Hohenheim (Stuttgart)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Stuttgart",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Trier",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Theologische Fakultät Trier",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Universität Trier",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Hochschule für Musik Trossingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Eberhard Karls Universität Tübingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Evangelische Hochschule für Kirchenmusik Tübingen",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Ulm",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Universität Ulm",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Philosophisch-Theologische Hochschule Vallendar",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "WHU – Otto Beisheim School of Management (Vallendar)",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Fachhochschule für Wirtschaft und Technik Vechta/Diepholz/Oldenburg",
    "bundesland": "Niedersachsen"
}, {
    "name": "Universität Vechta",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule für Polizei Baden-Württemberg",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Fachhochschule Wedel",
    "bundesland": "Schleswig-Holstein"
}, {
    "name": "Hochschule Weihenstephan-Triesdorf",
    "bundesland": "Bayern"
}, {
    "name": "Gustav-Siewerth-Akademie (Weilheim)",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Bauhaus-Universität Weimar",
    "bundesland": "Thüringen"
}, {
    "name": "Hochschule für Musik Franz Liszt Weimar",
    "bundesland": "Thüringen"
}, {
    "name": "Pädagogische Hochschule Weingarten",
    "bundesland": "Baden-Württemberg"
}, {
    "name": "Hochschule Harz (Wernigerode und Halberstadt)",
    "bundesland": "Sachsen-Anhalt"
}, {
    "name": "EBS Universität für Wirtschaft und Recht (Wiesbaden, Oestrich-Winkel)",
    "bundesland": "Hessen"
}, {
    "name": "Hessische Hochschule für Polizei und Verwaltung (Wiesbaden)",
    "bundesland": "Hessen"
}, {
    "name": "Hochschule RheinMain (Wiesbaden und Rüsselsheim)",
    "bundesland": "Hessen"
}, {
    "name": "Wilhelm Büchner Hochschule (Pfungstadt)",
    "bundesland": "Hessen"
}, {
    "name": "Technische Hochschule Wildau (FH)",
    "bundesland": "Brandenburg"
}, {
    "name": "Jade Hochschule (Wilhelmshaven, Oldenburg, Elsfleth)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Wismar",
    "bundesland": "Mecklenburg-Vorpommern"
}, {
    "name": "Universität Witten/Herdecke",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Ostfalia Hochschule für angewandte Wissenschaften (Wolfenbüttel, Salzgitter, Wolfsburg, Suderburg)",
    "bundesland": "Niedersachsen"
}, {
    "name": "Hochschule Worms",
    "bundesland": "Rheinland-Pfalz"
}, {
    "name": "Bergische Universität Wuppertal",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Kirchliche Hochschule Wuppertal/Bethel",
    "bundesland": "Nordrhein-Westfalen"
}, {
    "name": "Hochschule für angewandte Wissenschaften Würzburg-Schweinfurt",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule für Musik Würzburg",
    "bundesland": "Bayern"
}, {
    "name": "Julius-Maximilians-Universität Würzburg",
    "bundesland": "Bayern"
}, {
    "name": "Hochschule Zittau/Görlitz",
    "bundesland": "Sachsen"
}, {
    "name": "DPFA Hochschule Sachsen (Zwickau)",
    "bundesland": "Sachsen"
}, {
    "name": "NBS Northern Business School – Hochschule für Management und Sicherheit",
    "bundesland": "Hamburg"
}, {
    "name": "Westsächsische Hochschule Zwickau",
    "bundesland": "Sachsen"
}];
