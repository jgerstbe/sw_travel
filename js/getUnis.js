//1. open https://de.wikipedia.org/wiki/Liste_der_Hochschulen_in_Deutschland
//2. mark table with id="target"
//3. run script in console
//4. export log
//5. Format as array of objects --> cities.json
var unis = [];
$('#target tr').each(function() {
  $(this).map(function() {
    var $cells = $(this).children();
    var data = {
      name: $cells.eq(0).text(),
      bundesland: $cells.eq(1).text()
    };
    unis.push(data);
  })
});
$('body').html(JSON.stringify(unis));
