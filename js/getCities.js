//1. open https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9F-_und_Mittelst%C3%A4dte_in_Deutschland#Gro.C3.9F-_und_Mittelst.C3.A4dte_nach_Einwohnerzahl
//2. mark table with id="target"
//3. run script in console
//4. export log
//5. Format as array of objects --> cities.json
var cities = [];
$('#target tr').each(function() {
  $(this).map(function() {
    var $cells = $(this).children();
    var data = {
      name: $cells.eq(1).text(),
      bundesland: $cells.eq(7).text(),
      einwohner: $cells.eq(8).text(),
      rank: $cells.eq(0).text(),
    };
    cities.push(data);
  })
});
$('body').html(JSON.stringify(cities));
