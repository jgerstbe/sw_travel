var http = require("https");
var fs = require('fs');

var city = "Berlin";
var page = 1;
var url = "/api/v2/trips?fn=" + city + "&locale=de_DE&_format=json&cur=EUR&page=" + page + "&sort=trip_price&order=desc&limit=100&radius=10";
var options = {
    "method": "GET",
    "hostname": "public-api.blablacar.com",
    "port": null,
    "path": url,
    "headers": {
        "accept": "application/json",
        "key": "219dea236cfb40cfb1bc3e64558c1471"
    }
};
var routes = [];
var pages = 1;
for (var p = 0; p < 18; p++) {
    var req = http.request(options, function(res) {
        var chunks = [];

        res.on("data", function(chunk) {
            chunks.push(chunk);
        });

        res.on("end", function() {
            var body = JSON.parse(Buffer.concat(chunks).toString());
            pages = body.pager.pages;
            console.log(pages);
            console.log(body.trips.length)
            for (var i = 0; i < body.trips.length; i++) {
                let route = {
                    typ: "Carsharing",
                    anbieter: "BlaBlaCar",
                    linie: body.trips[i].permanent_id,
                    cities: body.trips[i].locations_to_display
                };
                routes.push(route);
                console.log(route.linie);
            }
            console.warn(body.pager.page + "/" + pages);
        });
    });

    req.end();
    page++;
}

console.log(pages);
