package model;

import java.util.ArrayList;

/**
 * Created by Jan-Erik on 23.06.2017.
 */
public class Route {

    String typ = new String();
    String anbieter = new String();
    String linie = new String();
    ArrayList<Integer> haltestellen = new ArrayList<Integer>();
    ArrayList<String> cities = new ArrayList<String>();

    public Route(String typ, String anbieter, String linie, ArrayList<Integer> haltestellen, ArrayList<String> cities) {
        this.typ = typ;
        this.anbieter = anbieter;
        this.linie = linie;
        this.haltestellen = haltestellen;
        this.cities = cities;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getAnbieter() {
        return anbieter;
    }

    public void setAnbieter(String anbieter) {
        this.anbieter = anbieter;
    }

    public String getLinie() {
        return linie;
    }

    public void setLinie(String linie) {
        this.linie = linie;
    }

    public ArrayList<Integer> getHaltestellen() {
        return haltestellen;
    }

    public void setHaltestellen(ArrayList<Integer> haltestellen) {
        this.haltestellen = haltestellen;
    }

    public ArrayList<String> getCities() {
        return cities;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
    }
}
