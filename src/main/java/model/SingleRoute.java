package model;

/**
 * Created by Jan-Erik on 25.06.2017.
 */
public class SingleRoute {
    String typ = new String();
    String anbieter = new String();
    String linie = new String();
    String start = new String();
    String ziel = new String();

    public SingleRoute(String typ, String anbieter, String linie, String start, String ziel) {

        this.typ = typ;
        this.anbieter = anbieter;
        this.linie = linie;
        this.start = start;
        this.ziel = ziel;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getAnbieter() {
        return anbieter;
    }

    public void setAnbieter(String anbieter) {
        this.anbieter = anbieter;
    }

    public String getLinie() {
        return linie;
    }

    public void setLinie(String linie) {
        this.linie = linie;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getZiel() {
        return ziel;
    }

    public void setZiel(String ziel) {
        this.ziel = ziel;
    }

}
