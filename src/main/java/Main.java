import java.io.*;
import java.util.ArrayList;
import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.XSD;
import org.json.JSONArray;
import org.json.JSONObject;
import model.Route;
import model.SingleRoute;
import parse.*;


/**
 * Created by Jan-Erik on 22.06.2017.
 */
public class Main {

    public static void main(final String[] args) throws Exception {

        //String source = "https://bitbucket.org/jgerstbe/sw_travel/";
        String baseURI = "https://bitbucket.org/jgerstbe/sw_travel/#";

        OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RULE_INF);
        model.setNsPrefix("jg", baseURI);

        OntClass city = model.createClass(baseURI+"city");
        city.addLabel("Deutsche Mittel- oder Großstadt", "de");
        city.addLabel("German city", "de");

        OntClass university = model.createClass(baseURI+"university");
        university.addLabel("Universität bzw. Hochschule", "de");
        university.addLabel("Institution of higher education", "de");

        OntClass route = model.createClass(baseURI+"route");
        route.addLabel("Fernverkehrsroute", "de");
        route.addLabel("long distance travel route", "de");

        OntClass connection = model.createClass(baseURI+"connection");
        connection.addLabel("Fernverkehrsverbindung", "de");
        connection.addLabel("long distance travel connection", "de");

        DatatypeProperty rank = model.createDatatypeProperty(baseURI+"rank");
        rank.addComment("Platzierung in der Liste der Städte in Deutschland mit mindestens 20.000 Einwohnern", "de");
        rank.addComment("Placement in ranking of cities with 20.000 or more citizens in Germany", "en");
        rank.addRange(XSD.xint);
        rank.addDomain(city);

        DatatypeProperty name = model.createDatatypeProperty(baseURI+"name");
        name.addComment("Name einer Stadt oder Unniversität/Hochschule", "de");
        name.addComment("Name of an city or institution of higher education", "en");
        name.addRange(XSD.xstring);
        name.addDomain(university);
        name.addDomain(city);

        DatatypeProperty bundesland = model.createDatatypeProperty(baseURI+"bundesland");
        bundesland.addComment("Bundesland der Bundesrepublik Deutschland", "de");
        bundesland.addComment("State of the Federal Republic of Germany", "en");
        bundesland.addRange(XSD.xstring);
        bundesland.addDomain(university);
        bundesland.addDomain(city);

        DatatypeProperty einwohner = model.createDatatypeProperty(baseURI+"einwohner");
        einwohner.addLabel("Einwohner", "de");
        einwohner.addLabel("Citizens", "en");
        einwohner.addRange(XSD.xint);
        einwohner.addDomain(city);

        DatatypeProperty typ = model.createDatatypeProperty(baseURI+"typ");
        typ.addComment("Definiert den Typ der Transportation (Auto, Bus, Zug)", "de");
        typ.addComment("Defines the type of transportation used (car, bus, train)", "en");
        typ.addRange(XSD.xstring);
        typ.addDomain(connection);

        DatatypeProperty anbieter = model.createDatatypeProperty(baseURI+"anbieter");
        anbieter.addComment("Unternehmen, dass die Reiseverbindung anbietet", "de");
        anbieter.addComment("Company that offers the travel connection", "en");
        anbieter.addRange(XSD.xstring);
        anbieter.addDomain(connection);

        DatatypeProperty line = model.createDatatypeProperty(baseURI+"linie");
        line.addComment("Fernverkehrslinie", "de");
        line.addComment("Long-distance line", "en");
        line.addRange(XSD.xstring);
        line.addDomain(connection);

        ObjectProperty start = model.createObjectProperty(baseURI+"start");
        start.addComment("Haltestelle einer Route, die als Startpunkt betrachtet wird.", "de");
        start.addComment("Stop of a route, which is considered a starting point.", "en");
        start.addDomain(connection);
        start.addRange(city);

        ObjectProperty destination = model.createObjectProperty(baseURI+"destination");
        destination.addComment("Haltestelle einer Route, die als Ziel betrachtet wird.", "de");
        destination.addComment("Stop of a route, which is considered a destination.", "en");
        destination.addDomain(connection);
        destination.addRange(city);

        ObjectProperty station = model.createObjectProperty(baseURI+"station");
        station.addComment("Haltestelle einer Route.", "de");
        station.addComment("Stop of a route.", "en");
        station.addDomain(route);
        station.addRange(city);

        ObjectProperty stop_of_route = model.createObjectProperty(baseURI+"stop_of_route");
        stop_of_route.addDomain(connection);
        stop_of_route.addRange(route);

        ObjectProperty ist_in = model.createObjectProperty(baseURI+"ist_in");
        ist_in.addDomain(university);
        ist_in.addRange(city);

        //JSONArray unis = loadJsonFile("js/unis_parsed.json");
        JSONArray unis = loadFile.getJSONArray("js/unis_parsed2.json");
        JSONArray cities = loadFile.getJSONArray("js/cities.json");

        for (int i=0; i< cities.length(); i++) {
            JSONObject obj = cities.getJSONObject(i);
            Individual stadt = model.createIndividual(makeIri(baseURI + obj.get("name")), city);
            stadt.addProperty(name, obj.get("name").toString());
            stadt.addProperty(bundesland, obj.get("bundesland").toString());
            stadt.addProperty(einwohner, obj.get("einwohner").toString());
            stadt.addProperty(rank, obj.get("rank").toString());
        }

        for (int i=0; i< unis.length(); i++) {
            JSONObject obj = unis.getJSONObject(i);
            Individual uni = model.createIndividual(makeIri(baseURI + obj.get("name")),university);
            uni.addProperty(name, obj.get("name").toString());
            uni.addProperty(bundesland, obj.get("bundesland").toString());
            System.out.println("Uni "+i+" ist in "+obj.get("stadt"));
            Resource r = model.createResource(makeIri(baseURI + obj.get("stadt").toString()));
            uni.addProperty(ist_in, r);
        }

        //Allgemeine Routen
        ArrayList<Route> routes = parseGTFS.getRoutes("db");
        routes.addAll(parseGTFS.getRoutes("flix"));
        routes.addAll( BlaBlaCar.parse(cities));
        for (Route r:routes) {
            Individual Route = model.createIndividual(makeIri(baseURI + r.getAnbieter()+"-"+r.getLinie()),route);
            Route.addProperty(typ, r.getTyp());
            Route.addProperty(anbieter, r.getAnbieter());
            Route.addProperty(line, r.getLinie());
            for (String s:r.getCities()) {
                Resource resStop = model.createResource(makeIri(baseURI + s));
                Route.addProperty(station, resStop);
            }
            String string = "Route: "+r.getAnbieter()+" - "+r.getLinie();
            System.out.println(string);
        }

        //Einzelne Verbindungen
        ArrayList<SingleRoute> singleRoutes = parseGTFS.createSingleRoutes(routes);
        for (SingleRoute r:singleRoutes) {
        //for (int o=0; o <2;o++) {
        //    SingleRoute r = singleRoutes.get(o);
            Individual con = model.createIndividual(makeIri(baseURI + r.getLinie()+"-"+r.getStart()+"-"+r.getZiel()),connection);
            Resource conRoute = model.createResource(makeIri(baseURI + r.getAnbieter()+"-"+r.getLinie()));
            con.addProperty(stop_of_route, conRoute);
            Resource resStart = model.createResource(makeIri(baseURI + r.getStart()));
            con.addProperty(start, resStart);
            Resource resDest = model.createResource(makeIri(baseURI + r.getZiel()));
            con.addProperty(destination, resDest);
            String string = "SingleRoute: "+r.getAnbieter()+" - "+r.getLinie()+" - "+r.getStart()+" - "+r.getZiel();
            System.out.println(string);
        }

        model.write(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("results/model.rdf"), "UTF8")));
    }

    public static String makeIri(String s) {
        return s.replace(" ", "_").replace(",", "");
    }
}

