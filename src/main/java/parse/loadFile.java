package parse;

import org.json.JSONArray;
import org.json.JSONObject;
import sun.applet.Main;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jan-Erik on 24.06.2017.
 */
public class loadFile {


    public static ArrayList<String[]> getLines(String fileName) {
        ArrayList<String[]> lines = new ArrayList<String[]>();

        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line !=null) {
                lines.add(line.split(","));
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    public static JSONArray getJSONArray(String fileName) {

        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line !=null) {
                sb.append(line);
                line = br.readLine();
            }

            JSONArray parsed = new JSONArray(sb.toString());
            return parsed;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getJSONfromURL(String url) {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new URL(url).openStream()));

            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line !=null) {
                sb.append(line);
                line = br.readLine();
            }

            JSONObject parsed = new JSONObject(sb.toString());
            return parsed;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void writeFile(String path, String data) {
        try{
            PrintWriter writer = new PrintWriter(path, "UTF-8");
            writer.println(data);
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
}
