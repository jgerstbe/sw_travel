package parse;

import model.Route;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by Jan-Erik on 29.06.2017.
 */
public class BlaBlaCar {

    public static ArrayList<Route> parse(JSONArray list) throws Exception {
        ArrayList<Route> routes = new ArrayList<Route>();
        JSONArray json = new JSONArray();

        for (int j = 0; j < list.length(); j++) {
            String city = list.getJSONObject(j).getString("name").replace(" ", "%20");
            int page = 1;
            String url = "https://public-api.blablacar.com/api/v2/trips?locale=de_DE&_format=json&cur=EUR&sort=trip_price&order=desc&limit=100&radius=10&key=219dea236cfb40cfb1bc3e64558c1471&fn=";
            int pages = loadFile.getJSONfromURL(url+city+"&page=1").getJSONObject("pager").getInt("pages");
            System.out.println("BlaBlaCar: "+city+" hat "+pages+" Seiten");
            for (int p = 1; p <= 1; p++) {//pages; p++) {
                page = p;
                JSONObject response = loadFile.getJSONfromURL(url+city+"&page="+page);
                JSONArray trips = response.getJSONArray("trips");
                for (int i = 0; i < trips.length(); i++) {
                    JSONObject o = trips.getJSONObject(i);
                    String linie = o.getString("permanent_id");
                    ArrayList<String> cities = new ArrayList<String>();
                    for (int k = 0; k < o.getJSONArray("locations_to_display").length(); k++) {
                        cities.add(o.getJSONArray("locations_to_display").get(k).toString());
                    }
                    System.out.println(linie+": "+cities.size());
                    routes.add(new Route("Carsharing", "BlaBlaCar", linie, null, cities));
                    //persistence
                    JSONObject jo = new JSONObject();
                    jo.put("name", list.getJSONObject(j).getString("name"));
                    jo.put("cities", cities.toArray().toString());
                    json.put(jo);
                }
            }
            loadFile.writeFile("js/blablacar.json", json.toString());
        }

        return routes;
    }
}
