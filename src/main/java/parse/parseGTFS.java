package parse;

import model.Route;
import model.SingleRoute;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jan-Erik on 24.06.2017.
 */
public class parseGTFS {

    public static ArrayList<Route> getRoutes(String src) {
        System.out.println("praseGTFS("+src+")");
        ArrayList<String[]> routes = loadFile.getLines("gtfs/"+src+"/routes.txt");
        ArrayList<String[]> trips = loadFile.getLines("gtfs/"+src+"/trips.txt");
        ArrayList<String[]> stopTimes = loadFile.getLines("gtfs/"+src+"/stop_times.txt");
        ArrayList<String[]> stops = loadFile.getLines("gtfs/"+src+"/stops.txt");
        ArrayList<Route> allRoutes = new ArrayList<Route>();
        String typ = src.toUpperCase();

        for (int i = 1; i < routes.size(); i++) {
            String routeId = routes.get(i)[0], tripId = new String();
            String name = routes.get(i)[2];
            String anbieter = routes.get(i)[4];
            //finde die tripId
            for (int j = 1; j < trips.size(); j++) {
                if (trips.get(j)[0].equals(routeId)) {
                    tripId = trips.get(j)[2];
                    break;
                }
            }
            //erstelle eine Liste der Haltestellen
            ArrayList<Integer> haltestellen = new ArrayList<Integer>();
            for (int k = 1; k < stopTimes.size(); k++) {
                if (stopTimes.get(k)[0].equals(tripId)) {
                    int stop_id = Integer.valueOf(stopTimes.get(k)[3]);
                    //int stop_sequence = Integer.valueOf(stopTimes.get(k)[4]);
                    haltestellen.add(stop_id);
                }
            }
            //löse die Haltestellen zu Städten auf
            ArrayList<String> cities = new ArrayList<String>();
            for (int halt:haltestellen) {
                for (int l = 1; l < stops.size(); l++) {
                    if (Integer.parseInt(stops.get(l)[0]) == halt) {
                        if (src.equals("db")) {
                            cities.add(matchCities(stops.get(l)[1]));
                        } else if (src.equals("flix")) {
                            cities.add(matchCities(stops.get(l)[2].substring(1)));
                            anbieter = "Flixbus";
                            typ = "Fernbus";
                            name = (int)(Math.random()*1000)+"";
                        }
                        break;
                    }
                }
            }
            allRoutes.add(new Route(typ, anbieter, name, haltestellen, cities));
        }
        return allRoutes;
        //return createSingleRoutes(allRoutes);
    }

    public static String matchCities(String name) {
        JSONArray jsonCities = loadFile.getJSONArray("js/cities.json");
        ArrayList<String> cities = new ArrayList<String>();
        for (int i = 0; i< jsonCities.length(); i++) {
            JSONObject city = jsonCities.getJSONObject(i);
            cities.add(city.get("name").toString());
        }
        name = name.replace("(", " (");
        name = name.replace(" Hbf", "");
        name = name.replace(" West", "");
        name = name.replace(" Süd", "");
        if (name.contains("Frankfurt")) {
            if (name.contains("Oder")) {
                return makeIri("Frankfurt (Oder)");
            } else {
                return makeIri("Frankfurt am Main");
            }
        }
        if (name.contains("Cologne")) {
            return makeIri("Köln");
        }
        if (name.contains("Munich")) {
            return makeIri("München");
        }
        if (name.contains("Nuremberg")) {
            return makeIri("Nürnberg");
        }
        if (name.contains("Hanover")) {
            return makeIri("Hannover");
        }
        if (name.contains("Coblenz")) {
            return makeIri("Koblenz");
        }
        if (name.contains("Constace")) {
            return makeIri("Konstanz");
        }
        if (name.contains("Zurich")) {
            return makeIri("Zürich");
        }
        if (name.contains("Muhlheim")) {
            return makeIri("Mühlheim am Main");
        }
        for(String c:cities) {
            if (c.startsWith(name)) {
                return makeIri(c);
            } else if (name.contains(c)) {
                return makeIri(c);
            }
        }
        //System.out.println("notfound: "+name);
        return makeIri(name);
    }

    public static String makeIri(String s) {
        return s.replace(" ", "_").replace(",", "").replace("/","-");
    }

    public static ArrayList<SingleRoute> createSingleRoutes(ArrayList<Route> routes) {
        JSONArray jsonCities = loadFile.getJSONArray("js/cities.json");
        ArrayList<String> cities = new ArrayList<String>();
        for (int i = 0; i< jsonCities.length(); i++) {
            JSONObject city = jsonCities.getJSONObject(i);
            cities.add(city.get("name").toString());
        }

        ArrayList<SingleRoute> singles = new ArrayList<SingleRoute>();
        for (Route r:routes) {
            ArrayList<String> cleanedCityList = new ArrayList<String>();
            for (String s:r.getCities()) {
                if (!cleanedCityList.contains(s)) {
                    cleanedCityList.add(s);
                }
            }
            for (int i=0; i < cleanedCityList.size(); i++) {
                for(String c:cleanedCityList) {
                    if (!c.equals(cleanedCityList.get(i))) {
                        if (cities.contains(c) && cities.contains(cleanedCityList.get(i))) {
                            singles.add(new SingleRoute(r.getTyp(), r.getAnbieter(), r.getLinie(),cleanedCityList.get(i), c));
                        }
                    }
                }
            }
        }
        return singles;
    }

}
